// To parse this JSON data, do
//
//     final tesCovid = tesCovidFromJson(jsonString);

import 'dart:convert';

class Artikel {
  Artikel({
    required this.model,
    required this.pk,
    required this.fields,
  });

  final String model;
  final int pk;
  final dynamic fields;

  factory Artikel.fromJson(Map<String, dynamic> json) => Artikel(
        model: json["model"],
        pk: json["pk"],
        fields: json["fields"],
      );
}

List<Artikel> parseArtikels(String responseBody) {
  final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();
  return parsed.map<Artikel>((json) => Artikel.fromJson(json)).toList();
}
