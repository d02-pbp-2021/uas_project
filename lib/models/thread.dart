import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Thread {
    final String topic;
    final String slug;
    final String name;
    final String date_created;
    final String content;

    const Thread ({
        @required this.topic = "topic",
        @required this.slug = "slug",
        @required this.name = "name",
        @required this.date_created = "date",
        @required this.content = "content",
    });

    String toString() {
        return "Topic: " + this.topic + "\n" +
            "Slug: " + this.slug + "\n" +
            "Name: " + this.name + "\n" +
            "Date Created: " + this.date_created + "\n" +
            "Content: " + this.content + "\n";
    }

    factory Thread.fromJson(Map<String, dynamic> json) {
        return Thread(
            topic: json['topic'],
            slug: json['slug'],
            name: json['name'],
            date_created: json['date_created'],
            content: json['content'],
        );
    }

    static Future<List<Thread>> getThread(String index) async {
        // print(index);
        var response = await http.post(
            Uri.parse("https://lindungipeduli.herokuapp.com/load-flutter/"),
            headers: {
                'Content-Type': 'application/json; charset=UTF-8',
            },
            body: jsonEncode(<String, String>{
                "index": index,
            }),
        );
        // print("Finish Post");
        // print(response.body);
        // print("=====================================================\n");

        var jsonObject = json.decode(response.body);
        // print("jsonObject CLEAR\n");
        List<dynamic> listThread = json.decode(jsonObject['show']);
        // print("listThread CLEAR\n");

        List<Thread> threads= [];
        for (int i = 0; i < listThread.length; i++) {
            threads.add(
                Thread.fromJson(listThread[i]['fields'])
            );
            // print(threads[i].toString());
        }
        return threads;
    }
}