import 'dart:convert';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:uas_project/apotek_online/widgets/apotek_card_widget.dart';
import 'package:uas_project/apotek_online/models/apotek_model.dart';
import 'package:uas_project/apotek_online/screens/apotek_form_page.dart';
import 'package:uas_project/widgets/drawer.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/foundation.dart';
import '../models/apotek_model.dart';
import 'package:uas_project/pengaturan_pengguna/login_page.dart';

class KatalogApotek extends StatefulWidget {
  const KatalogApotek({
    Key? key,
  }) : super(key: key);

  @override
  State<KatalogApotek> createState() => _KatalogApotekState();
}

class _KatalogApotekState extends State<KatalogApotek> {
  String? query;
  TextEditingController search_controller = TextEditingController();
  Future<List<Product>> futureProduct = fetch_products(http.Client(), "");
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return Scaffold(
      appBar: AppBar(
        title: Text("Apotek Daring"),
        backgroundColor: Colors.blueGrey,
      ),
      drawer: MyDrawer(),
      floatingActionButton: Visibility(
        child: FloatingActionButton(
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return FormObat();
            }));
          },
          backgroundColor: Colors.indigo,
          child: Icon(Icons.add),
        ),
        visible: (LoginPage.roles == "Admin" || LoginPage.roles == "Seller")
            ? true
            : false,
      ),
      body: FutureBuilder<List<Product>>(
        future: futureProduct,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text('${snapshot.error}');
          } else if (snapshot.hasData) {
            return CustomScrollView(
              slivers: [
                SliverList(
                    delegate: SliverChildListDelegate.fixed([
                  Container(
                    // Heading tulisan ApotekDaring
                    height: 100,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Apotek",
                          style: TextStyle(
                              fontSize: 30,
                              color: Colors.amber,
                              fontWeight: FontWeight.w700),
                        ),
                        Text(
                          "Daring",
                          style: TextStyle(
                              fontSize: 30,
                              color: Colors.indigo,
                              fontWeight: FontWeight.w700),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    // Bagian search bar
                    margin: EdgeInsets.all(10),
                    child: TextField(
                      controller: search_controller,
                      onChanged: (value) {
                        setState(() {
                          query = search_controller.text;
                          futureProduct = fetch_products(http.Client(), query);
                        });
                      },
                      decoration: InputDecoration(
                        suffixIcon: Icon(Icons.search),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                        hintText: "Cari produk...",
                      ),
                    ),
                  ),
                ])),
                SliverGrid(
                    // product cards
                    delegate: SliverChildBuilderDelegate((ctx, index) {
                      return ProductCard(
                          imageURL: snapshot.data![index].fields["link_gambar"],
                          namaObat: snapshot.data![index].fields["nama_obat"],
                          descObat: snapshot.data![index].fields["khasiat"],
                          linkProduk:
                              snapshot.data![index].fields["link_produk"]);
                    }, childCount: snapshot.data!.length),
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2, childAspectRatio: 5 / 7))
              ],
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}

// Referensi:
// - https://docs.flutter.dev/cookbook/networking/fetch-data
// - https://youtu.be/oFZIwBudIj0
// - https://youtu.be/s9_RWKMc1GY
// - https://youtu.be/9vh3jaTgT9k

