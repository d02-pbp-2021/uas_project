import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:uas_project/apotek_online/models/apotek_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:uas_project/apotek_online/widgets/apotek_card_widget.dart';
import 'apotek_main_page.dart';
import 'package:http/http.dart' as http;

class FormObat extends StatefulWidget {
  const FormObat({Key? key}) : super(key: key);

  @override
  State<FormObat> createState() => _FormObatState();
}

class _FormObatState extends State<FormObat> {
  TextEditingController nama_obat_controller = TextEditingController();
  TextEditingController deskripsi_controller = TextEditingController();
  TextEditingController link_produk_controller = TextEditingController();
  TextEditingController link_gambar_controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return Scaffold(
      body: Center(
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Container(
              // heading TambahObat
              height: 100,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Tambah",
                    style: TextStyle(
                        fontSize: 30,
                        color: Colors.amber,
                        fontWeight: FontWeight.w700),
                  ),
                  Text(
                    "Obat",
                    style: TextStyle(
                        fontSize: 30,
                        color: Colors.indigo,
                        fontWeight: FontWeight.w700),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.all(10),
              child: TextField(
                // field nama produk
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  labelText: "Nama Produk",
                  hintText: "Panadol",
                ),
                controller: nama_obat_controller,
                onChanged: (value) {
                  setState(() {});
                },
              ),
            ),
            Container(
              margin: EdgeInsets.all(10),
              child: TextField(
                // field deskripsi
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  labelText: "Deskripsi",
                  hintText: "Meringankan sakit kepala",
                ),
                controller: deskripsi_controller,
                onChanged: (value) {
                  setState(() {});
                },
              ),
            ),
            Container(
              margin: EdgeInsets.all(10),
              child: TextField(
                // field link produk
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  labelText: "Link produk",
                  hintText: "http://panadol.com",
                ),
                controller: link_produk_controller,
                onChanged: (value) {
                  setState(() {});
                },
              ),
            ),
            Container(
              // field link gambar
              margin: EdgeInsets.all(10),
              child: TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  labelText: "Link Gambar",
                  hintText: "http://panadol.jpg",
                ),
                controller: link_gambar_controller,
                onChanged: (value) {
                  setState(() {});
                },
              ),
            ),
            Container(
              // button tambahkan
              margin: EdgeInsets.all(10),
              child: RaisedButton(
                color: Colors.indigo,
                child: Text(
                  "Tambahkan",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
                onPressed: () async {
                  // post
                  create_product(
                      nama_obat_controller.text,
                      deskripsi_controller.text,
                      link_produk_controller.text,
                      link_gambar_controller.text);
                  // Navigator push
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) {
                    return KatalogApotek();
                  }));
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Future<void> create_product(String nama_obat, String deskripsi,
    String link_produk, String link_gambar) async {
  http
      .post(
        Uri.parse('https://lindungipeduli.herokuapp.com/apotek/post/'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          "nama_obat": nama_obat,
          "khasiat": deskripsi,
          "link_produk": link_produk,
          "link_gambar": link_gambar
        }),
      )
      .then((response) => print(response.body))
      .catchError((error) => print(error));
}

// Referensi:
// - https://docs.flutter.dev/cookbook/networking/send-data
// - https://youtu.be/mq5cL92IBgI
// - https://youtu.be/aHT7v_4UBtA
// - https://youtu.be/O7yaoGRNY2E
