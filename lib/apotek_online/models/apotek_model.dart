import 'dart:convert';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:flutter/foundation.dart';

class Product {
  final String model;
  final int pk;
  final dynamic fields;

  const Product({
    required this.model,
    required this.pk,
    required this.fields,
  });

  factory Product.fromJson(Map<String, dynamic> json) {
    return Product(
        model: json["model"], pk: json["pk"], fields: json["fields"]);
  }
}

List<Product> parseProducts(String responseBody) {
  final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();
  return parsed.map<Product>((json) => Product.fromJson(json)).toList();
}

Future<List<Product>> fetch_products(http.Client client, String? query) async {
  final response = await client
      .get(Uri.parse("https://lindungipeduli.herokuapp.com/apotek/json/"));
  List<Product> productList;
  if (response.statusCode == 200) {
    productList = parseProducts(response.body).where((element) {
      final q = query!.toLowerCase();
      final nama = element.fields["nama_obat"].toString().toLowerCase();

      return nama.contains(q);
    }).toList();
    return productList;
  } else {
    throw Exception("gagal");
  }
}

// Referensi:
// - https://docs.flutter.dev/cookbook/networking/fetch-data
// - https://docs.flutter.dev/cookbook/networking/background-parsing
// - https://youtu.be/oFZIwBudIj0