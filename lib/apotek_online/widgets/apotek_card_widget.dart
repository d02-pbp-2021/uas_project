import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

// Referensi: https://youtu.be/aR1o09jaocs
class ProductCard extends StatelessWidget {
  final String imageURL;
  final String namaObat;
  final String descObat;
  final String linkProduk;

  const ProductCard(
      {Key? key,
      required this.imageURL,
      required this.namaObat,
      required this.descObat,
      required this.linkProduk})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      // container luar
      height: MediaQuery.of(context).size.width * (0.7),
      width: MediaQuery.of(context).size.width * (0.5),
      child: Container(
        // container dalam
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.grey[100],
          borderRadius: BorderRadius.circular(16),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              // bagian atas (gambar, nama, deskripsi)
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  // gambar
                  height: MediaQuery.of(context).size.width * (0.3),
                  width: MediaQuery.of(context).size.width * (0.5),
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(16),
                      topRight: Radius.circular(16),
                    ),
                    child: Image(
                      image: NetworkImage(imageURL),
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                Container(
                  // nama obat
                  margin: EdgeInsets.all(5),
                  child: Text(
                    namaObat,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                Container(
                  // deskripsi
                  margin: EdgeInsets.all(5),
                  child: Text(
                    descObat,
                    style: TextStyle(
                      fontSize: 13,
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              // tombol redirect link pembelian
              width: MediaQuery.of(context).size.width * (0.5),
              child: RaisedButton(
                color: Colors.indigo,
                onPressed: () async {
                  await openUrl(linkProduk);
                },
                child: Icon(
                  Icons.add_shopping_cart,
                  size: 18,
                  color: Colors.white,
                ),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(16),
                  bottomRight: Radius.circular(16),
                )),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

// Referensi: https://youtu.be/8DgJnucRBss
Future<void> openUrl(String url,
    {bool forceWebView = false, bool enableJavaScript = false}) async {
  await launch(url);
}
