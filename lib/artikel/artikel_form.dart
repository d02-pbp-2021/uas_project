import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:select_form_field/select_form_field.dart';
import 'package:uas_project/artikel/artikel_home.dart';

class ArtikelForm extends StatefulWidget {
  const ArtikelForm({Key? key}) : super(key: key);

  @override
  _ArtikelFormState createState() => _ArtikelFormState();
}

class _ArtikelFormState extends State<ArtikelForm> {
  final _formKey = GlobalKey<FormState>();
  String? judul;
  String? ringkasan;
  String? isi;
  String? gambar;
  String? kategori;

  List<Kategori> daftarKategori = [];

  @override
  void initState() {
    getKategori();
    super.initState();
  }

  void getKategori() async {
    final kategoris = await fetchKategori();

    setState(() {
      daftarKategori = kategoris;
      update();
    });
  }

  List<Map<String, dynamic>> selectKategori = [];

  void update() {
    setState(() {
      for (int i = 0; i < daftarKategori.length; i++) {
        selectKategori.add({
          'label': daftarKategori[i].fields['nama'],
          'value': daftarKategori[i].fields['nama'],
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Tulis artikelmu"),
        backgroundColor: Colors.blueGrey,
      ),
      body: Container(
        padding: const EdgeInsets.all(20.0),
        child: Form(
          key: _formKey,
          child: Scrollbar(
            child: ListView(
              children: <Widget>[
                const SizedBox(height: 30),
                const Text('Gambar'),
                TextFormField(
                  decoration: const InputDecoration(
                    border: UnderlineInputBorder(),
                  ),
                  onChanged: (value) {
                    setState(() {
                      gambar = value;
                    });
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Gambar artikel belum diisi";
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 30),
                const Text('Judul'),
                TextFormField(
                  decoration: const InputDecoration(
                    border: UnderlineInputBorder(),
                  ),
                  onChanged: (value) {
                    setState(() {
                      judul = value;
                      // print(judul);
                    });
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Judul artikel belum diisi";
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 30),
                SelectFormField(
                  type: SelectFormFieldType.dropdown,
                  initialValue: "Pilih Kategori",
                  labelText: "Kategori",
                  items: selectKategori,
                  onChanged: (value) {
                    setState(() {
                      kategori = value;
                      // print(kategori);
                    });
                  },
                  validator: (value) {
                    if (value == null) {
                      return 'Kategori artikel belum diisi';
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 30),
                const Text('Ringkasan'),
                TextFormField(
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  decoration: const InputDecoration(
                    border: UnderlineInputBorder(),
                  ),
                  onChanged: (value) {
                    setState(() {
                      ringkasan = value;
                    });
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Ringkasan artikel belum diisi';
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 30),
                const Text('Isi'),
                TextFormField(
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  decoration: const InputDecoration(
                    border: UnderlineInputBorder(),
                  ),
                  onChanged: (value) {
                    setState(() {
                      isi = value;
                    });
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Isi artikel belum diisi';
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 40),
                ElevatedButton(
                    onPressed: () async {
                      String destination =
                          "https://lindungipeduli.herokuapp.com/artikel/post_data/";
                      var url = Uri.parse(destination);
                      if (_formKey.currentState!.validate()) {
                        final response = await http.post(url,
                            headers: {
                              'Content-Type': 'application/json; charset=UTF-8',
                              "Access-Control-Allow-Origin": "*"
                            },
                            body: json.encode({
                              'judul': judul,
                              'ringkasan': ringkasan,
                              'gambar': gambar,
                              'isi': isi,
                              'kategori': kategori,
                            }));
                        _formKey.currentState?.reset();
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const ArtikelScreen()));
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                                content: Text('Artikel gagal dikirim')));
                      }
                    },
                    child: const Text('Kirim'))
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class Kategori {
  final String model;
  final int pk;
  final dynamic fields;

  Kategori({
    required this.model,
    required this.pk,
    required this.fields,
  });

  factory Kategori.fromJson(Map<String, dynamic> json) {
    return Kategori(
        model: json['model'], pk: json['pk'], fields: json['fields']);
  }
}

List<Kategori> parseProducts(String responseBody) {
  final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();
  return parsed.map<Kategori>((json) => Kategori.fromJson(json)).toList();
}

Future<List<Kategori>> fetchKategori() async {
  http.Client client = http.Client();
  final response = await client.get(Uri.parse(
      "https://lindungipeduli.herokuapp.com/artikel/fetch_data_kategori/"));

  if (response.statusCode == 200) {
    return compute(parseProducts, response.body);
  } else {
    throw Exception("X");
  }
}
