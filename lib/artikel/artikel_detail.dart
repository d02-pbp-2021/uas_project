import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:uas_project/artikel/artikel_home.dart';
import 'package:uas_project/models/artikel.dart';
import 'package:http/http.dart' as http;
import 'package:uas_project/pengaturan_pengguna/login_page.dart';

class ArtikelDetail extends StatelessWidget {
  final Artikel artikel;
  const ArtikelDetail({Key? key, required this.artikel}) : super(key: key);

  Widget _deleteButton(context) {
    if (LoginPage.roles == "Author" || LoginPage.roles == "Admin") {
      return ElevatedButton(
          style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(Colors.white),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(28)))),
          // Within the `FirstRoute` widget
          onPressed: () async {
            showDialog(
                context: context,
                barrierDismissible: false,
                builder: (context) {
                  return AlertDialog(
                    title: const Text("HAPUS"),
                    content: const Text("Apakah anda yakin?"),
                    actions: [
                      TextButton(
                        child: const Text("No"),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      TextButton(
                          child: const Text("Yes"),
                          onPressed: () async {
                            String destination =
                                "https://lindungipeduli.herokuapp.com/artikel/post_delete_data/";
                            var url = Uri.parse(destination);
                            final response = await http.post(url,
                                body: json.encode({'pk': artikel.pk}));

                            Navigator.of(context).pop();
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        const ArtikelScreen()));
                          })
                    ],
                  );
                });
          },
          child: const Icon(
            Icons.delete,
            color: Colors.blue,
          ));
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Artikel"),
        backgroundColor: Colors.blueGrey,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Container(
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(28)),
          child: ListView(
            children: <Widget>[
              ClipRRect(
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(8), topRight: Radius.circular(8)),
                child: Image.network(artikel.fields['gambar']),
              ),
              const SizedBox(height: 10),
              Text(
                artikel.fields['judul'],
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                  color: Colors.grey[800],
                ),
              ),
              const SizedBox(height: 10),
              Text(
                artikel.fields['nama_penulis'] +
                    " • " +
                    artikel.fields['tanggal'],
                style: TextStyle(
                  fontSize: 10.0,
                  color: Colors.red[800],
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 10),
              Text(
                artikel.fields['kategori'],
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.grey[500],
                ),
              ),
              const SizedBox(height: 10),
              Text(
                artikel.fields['ringkasan'],
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.grey[800],
                ),
              ),
              const SizedBox(height: 10),
              // https://iamneil09.medium.com/how-to-draw-a-horizontal-line-in-flutter-b9aac700ce78
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: Container(
                  height: 1.0,
                  width: 130.0,
                  color: Colors.black,
                ),
              ),
              const SizedBox(height: 10),
              Text(
                artikel.fields['isi'],
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.grey[800],
                ),
              ),
              const SizedBox(height: 10),
              _deleteButton(context)
            ],
          ),
        ),
      ),
    );
  }
}
