import 'package:flutter/material.dart';
import 'package:uas_project/artikel/artikel_detail.dart';
import 'package:uas_project/artikel/artikel_kategori.dart';

Widget artikelCardTemplate(artikel, context) {
  return Card(
    elevation: 24,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(28)),
    shadowColor: Colors.black38,
    margin: const EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 8.0),
    child: Padding(
      padding: const EdgeInsets.all(15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Center(
            child: ClipRRect(
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(28),
                topRight: Radius.circular(28),
              ),
              child: Image.network(artikel.fields['gambar'],
                  height: 250, fit: BoxFit.cover),
            ),
          ),
          const SizedBox(height: 10),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Text(
              artikel.fields['judul'],
              style: TextStyle(
                fontSize: 20.0,
                color: Colors.grey[800],
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          const SizedBox(height: 5),
          ElevatedButton(
            style: ButtonStyle(
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(28)))),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ArtikelKategori(
                        querySearched: artikel.fields['kategori'])),
              );
            },
            child: Text(
              artikel.fields['kategori'],
              style: const TextStyle(
                fontSize: 14,
                color: Colors.white,
              ),
            ),
          ),
          const SizedBox(height: 5),
          Text(
            artikel.fields['ringkasan'],
            style: TextStyle(
              fontSize: 18,
              color: Colors.grey[800],
            ),
          ),
          const SizedBox(height: 10),
          Text(
            artikel.fields['nama_penulis'] + " • " + artikel.fields['tanggal'],
            style: TextStyle(
              fontSize: 10.0,
              color: Colors.red[800],
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 10),
          ElevatedButton(
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.white),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(28)))),
            // Within the `FirstRoute` widget
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ArtikelDetail(artikel: artikel)),
              );
            },
            child: const Text(
              'Baca selengkapnya',
              style: TextStyle(color: Colors.blue),
            ),
          )
        ],
      ),
    ),
  );
}

Widget artikelCarouselTemplate(artikel, context) {
  return GestureDetector(
    onTap: () {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ArtikelDetail(artikel: artikel)));
    },
    child: SizedBox(
      width: 344,
      child: Card(
        shadowColor: Colors.black38,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(28)),
        elevation: 12,
        child: Stack(
          fit: StackFit.loose,
          children: <Widget>[
            Center(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(28),
                child: Image.network(artikel.fields['gambar'],
                    width: 344, fit: BoxFit.cover),
              ),
            ),
            Positioned(
              bottom: 20,
              left: 20,
              child: Text(
                artikel.fields['judul'],
                style: const TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    overflow: TextOverflow.ellipsis),
              ),
            )
          ],
        ),
      ),
    ),
  );
}
