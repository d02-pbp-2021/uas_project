import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:uas_project/models/artikel.dart';
import 'package:uas_project/artikel/artikel_form.dart';
import 'package:uas_project/artikel/artikel_widget.dart';
import 'package:uas_project/pengaturan_pengguna/login_page.dart';
import 'package:http/http.dart' as http;
import 'dart:async';

class ArtikelSearch extends StatefulWidget {
  final String querySearched;
  const ArtikelSearch({Key? key, required this.querySearched})
      : super(key: key);

  @override
  _ArtikelSearchState createState() => _ArtikelSearchState();
}

class _ArtikelSearchState extends State<ArtikelSearch> {
  final ScrollController _scrollController = ScrollController();
  List<Artikel> myList = [];
  List<Artikel> currentList = [];
  String query = "";

  // appbar = https://blog.logrocket.com/how-to-create-search-bar-flutter/
  Icon customIcon = const Icon(Icons.search);
  Widget customSearchBar = const Text("Artikel");

  Future<List<Artikel>> fetchDataSearch(String? query) async {
    http.Client client = http.Client();
    final response = await client.get(
        Uri.parse("https://lindungipeduli.herokuapp.com/artikel/fetch_data/"));
    List<Artikel> artikels;

    if (response.statusCode == 200) {
      // print(parseArtikels(response.body));
      artikels = parseArtikels(response.body).where((element) {
        final queryLower = query!.toLowerCase();
        final judul = element.fields["judul"].toString().toLowerCase();
        final kategori = element.fields["kategori"].toString().toLowerCase();
        final ringkasan = element.fields["ringkasan"].toString().toLowerCase();
        final isi = element.fields["isi"].toString().toLowerCase();

        return judul.contains(queryLower) ||
            kategori.contains(queryLower) ||
            ringkasan.contains(queryLower) ||
            isi.contains(queryLower);
      }).toList();

      return artikels;
    } else {
      throw Exception("tet tot X");
    }
  }

  @override
  void initState() {
    fetchDataSearch(widget.querySearched).then((value) => {
          // print("value " + value.length.toString()),
          for (int i = 0; i < value.length; i++)
            {
              setState(() {
                myList.add(value[i]);
                if (i < 2) {
                  currentList.add(value[i]);
                }
              })
            }
        });

    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        int index = maxValue(currentList.length + 2);

        if (currentList.length <= myList.length) {
          setState(() {
            for (int i = currentList.length; i < index; i++) {
              currentList.add(myList[i]);
            }
          });
        }
      }
    });
    super.initState();
  }

  int maxValue(int index) {
    if (index > myList.length) {
      return myList.length;
    }

    return index;
  }

  String display() {
    if (widget.querySearched == "") {
      return "Anda tidak mencari apa-apa";
    } else {
      return "Anda mencari " + widget.querySearched;
    }
  }

  Widget _loading() {
    if (currentList.isEmpty) {
      return Center(
          child: Column(children: const <Widget>[
        SizedBox(height: 100),
        Text("Belum ditemukan"),
        SizedBox(height: 20),
        SizedBox(
          width: 30,
          height: 30,
          child: CircularProgressIndicator(),
        )
      ]));
    } else {
      return ListView.builder(
          physics: const ScrollPhysics(),
          shrinkWrap: true,
          itemCount: currentList.length,
          itemBuilder: (context, index) {
            if (index == currentList.length) {
              return const CupertinoActivityIndicator();
            }
            return artikelCardTemplate(currentList[index], context);
          });
    }
  }

  Widget _floatingActionButton() {
    if (LoginPage.roles == "Author" || LoginPage.roles == "Admin") {
      return FloatingActionButton(
        onPressed: () => Navigator.push(context,
            MaterialPageRoute(builder: (context) => const ArtikelForm())),
        child: const Icon(Icons.add),
      );
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        actions: [
          IconButton(
              onPressed: () {
                setState(() {
                  if (customIcon.icon == Icons.search) {
                    customIcon = const Icon(Icons.cancel);
                    customSearchBar = ListTile(
                      title: TextField(
                        onChanged: (value) {
                          setState(() {
                            query = value;
                          });
                        },
                        onSubmitted: (value) {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      ArtikelSearch(querySearched: query)));
                        },
                        decoration: const InputDecoration(
                            hintText: "Cari artikel...",
                            hintStyle: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                            ),
                            border: InputBorder.none),
                        style: const TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    );
                  } else {
                    customIcon = const Icon(Icons.search);
                    customSearchBar = const Text("Artikel");
                  }
                });
              },
              icon: customIcon)
        ],
        title: customSearchBar,
      ),
      floatingActionButton: _floatingActionButton(),
      body: ListView(
          controller: _scrollController,
          physics: const ClampingScrollPhysics(),
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 16, bottom: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 16),
                    child: Text(
                      display(),
                      style: const TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                          color: Color(0xFF3A3A3A)),
                    ),
                  )
                ],
              ),
            ),
            Padding(padding: const EdgeInsets.all(10), child: _loading()),
          ]),
    );
  }
}
