import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:uas_project/artikel/artikel_search.dart';
import 'package:uas_project/models/artikel.dart';
import 'package:uas_project/artikel/artikel_form.dart';
import 'package:uas_project/artikel/artikel_widget.dart';
import 'package:uas_project/pengaturan_pengguna/login_page.dart';
import 'package:uas_project/widgets/drawer.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'package:flutter/foundation.dart';

class ArtikelScreen extends StatelessWidget {
  const ArtikelScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.lightBlue[800],
        fontFamily: 'Fredoka One',
        textTheme: const TextTheme(
          headline6: TextStyle(fontSize: 24.0),
        ),
      ),
      home: const Scaffold(
        body: ArtikelHome(),
      ),
    );
  }
}

class ArtikelHome extends StatefulWidget {
  const ArtikelHome({Key? key}) : super(key: key);

  @override
  _ArtikelHomeState createState() => _ArtikelHomeState();
}

class _ArtikelHomeState extends State<ArtikelHome> {
  final ScrollController _scrollController = ScrollController();
  List<Artikel> myList = [];
  List<Artikel> currentList = [];
  String query = "";

  // appbar = https://blog.logrocket.com/how-to-create-search-bar-flutter/
  Icon customIcon = const Icon(Icons.search);
  Widget customSearchBar = const Text("Artikel");

  Future<List<Artikel>> fetchData() async {
    http.Client client = http.Client();
    final response = await client.get(
        Uri.parse("https://lindungipeduli.herokuapp.com/artikel/fetch_data/"));

    if (response.statusCode == 200) {
      return compute(parseArtikels, response.body);
    } else {
      throw Exception("X");
    }
  }

  Future<List<Artikel>> fetchDataTerpopuler() async {
    http.Client client = http.Client();
    final response = await client.get(Uri.parse(
        "https://lindungipeduli.herokuapp.com/artikel/fetch_data_terpopuler/"));

    if (response.statusCode == 200) {
      return compute(parseArtikels, response.body);
    } else {
      throw Exception("X");
    }
  }

  @override
  void initState() {
    fetchData().then((value) => {
          // print(value.length.toString()),
          for (int i = 0; i < value.length; i++)
            {
              setState(() {
                myList.add(value[i]);
                if (i < 2) {
                  currentList.add(value[i]);
                }
              })
            }
        });

    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        int index = maxValue(currentList.length + 2);

        if (currentList.length <= myList.length) {
          setState(() {
            for (int i = currentList.length; i < index; i++) {
              currentList.add(myList[i]);
            }
          });
        }
      }
    });
    super.initState();
  }

  int maxValue(int index) {
    if (index > myList.length) {
      return myList.length;
    }

    return index;
  }

  Widget _loading() {
    if (currentList.isEmpty) {
      return Center(
          child: Column(children: const <Widget>[
        SizedBox(height: 100),
        Text("Sedang mengambil data.."),
        SizedBox(height: 20),
        SizedBox(
          width: 30,
          height: 30,
          child: CircularProgressIndicator(),
        )
      ]));
    } else {
      return ListView.builder(
          physics: const ScrollPhysics(),
          shrinkWrap: true,
          itemCount: currentList.length,
          itemBuilder: (context, index) {
            if (index == currentList.length) {
              return const CupertinoActivityIndicator();
            }
            return artikelCardTemplate(currentList[index], context);
          });
    }
  }

  Widget _floatingActionButton() {
    if (LoginPage.roles == "Author" || LoginPage.roles == "Admin") {
      return FloatingActionButton(
        onPressed: () => Navigator.push(context,
            MaterialPageRoute(builder: (context) => const ArtikelForm())),
        child: const Icon(Icons.add),
      );
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        actions: [
          IconButton(
              onPressed: () {
                setState(() {
                  if (customIcon.icon == Icons.search) {
                    customIcon = const Icon(Icons.cancel);
                    customSearchBar = ListTile(
                      title: TextField(
                        onChanged: (value) {
                          setState(() {
                            query = value;
                          });
                        },
                        onSubmitted: (value) {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      ArtikelSearch(querySearched: query)));
                        },
                        decoration: const InputDecoration(
                            hintText: "Cari artikel...",
                            hintStyle: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                            ),
                            border: InputBorder.none),
                        style: const TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    );
                  } else {
                    customIcon = const Icon(Icons.search);
                    customSearchBar = const Text("Artikel");
                  }
                });
              },
              icon: customIcon)
        ],
        title: customSearchBar,
      ),
      floatingActionButton: _floatingActionButton(),
      drawer: const MyDrawer(),
      body: ListView(
          controller: _scrollController,
          physics: const ClampingScrollPhysics(),
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 16, bottom: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 16),
                    child: Text(
                      'Artikel Terpopuler',
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                          color: Color(0xFF3A3A3A)),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 250,
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: FutureBuilder<List<Artikel>>(
                  future: fetchDataTerpopuler(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          padding: EdgeInsets.all(10),
                          itemCount: 3,
                          itemBuilder: (context, index) {
                            return artikelCarouselTemplate(
                                snapshot.data![index], context);
                          });
                    } else if (snapshot.hasError) {
                      return Text('${snapshot.error}');
                    }

                    return Center(
                        child: Column(children: const <Widget>[
                      SizedBox(height: 100),
                      Text("Sedang mengambil data.."),
                      SizedBox(height: 20),
                      SizedBox(
                        width: 30,
                        height: 30,
                        child: CircularProgressIndicator(),
                      )
                    ]));
                  },
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 16, bottom: 6),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 16),
                    child: Text(
                      'Artikel Terbaru',
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                          color: Color(0xFF3A3A3A)),
                    ),
                  )
                ],
              ),
            ),
            Padding(padding: const EdgeInsets.all(10), child: _loading()),
          ]),
    );
  }
}
