import 'package:flutter/material.dart';
import 'package:uas_project/pengaturan_pengguna/login_page.dart';

class ThreadItem extends StatelessWidget {
    final String topic;
    final String slug;
    final String name;
    final String date_created;
    final String content;

    ThreadItem(this.topic, this.slug, this.name, this.date_created, this.content);

    @override
    Widget build(BuildContext context) {
        return Card(
            child: Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        colors: [
                            Colors.yellow,
                            Colors.orangeAccent,
                            Colors.yellow.shade300,
                        ],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                    ),
                ),
                child: Padding(
                    padding: (LoginPage.roles != '') ? EdgeInsets.fromLTRB(2,0,0,0) : EdgeInsets.fromLTRB(0,0,0,0),
                    child: Container(
                        color: Colors.white,
                        child: Padding(
                            padding: EdgeInsets.all(10),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                    Text(
                                        topic,
                                        style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                        "by " + name + " - " + date_created,
                                        style: TextStyle(fontSize: 12),
                                    ),
                                    const SizedBox(height: 12),
                                    Container(
                                        width: double.infinity,
                                        constraints: BoxConstraints(
                                            minHeight: 50,
                                        ),
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: Colors.blueGrey[100]!,
                                                width: 0.5,
                                            ),
                                            color: Colors.blueGrey[50]!,
                                        ),
                                        child: Padding(
                                            padding: EdgeInsets.all(5),
                                            child: Text(
                                                content,
                                                style: TextStyle(fontSize: 12),
                                            ),
                                        ),
                                    ),
                                ]
                            ),
                        ),
                    ),
                ),
            ),
        );
    }
}