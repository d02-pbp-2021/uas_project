import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:uas_project/widgets/drawer.dart';
import 'login_page.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage();
  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final GlobalKey<FormState> _regFormKey = GlobalKey<FormState>();
  bool isVisible = false;
  void toggleVisibility() {
    setState(() {
      isVisible = !isVisible;
    });
  }

  String username = "";
  String password1 = "";
  String password2 = "";
  String role = "";
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      drawer: MyDrawer(),
      appBar: AppBar(
        title: Text("Lindungi Peduli"),
          backgroundColor: Colors.blueGrey,
      ),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.fromLTRB(50, 50, 50, 50),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Register Your Account",
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: "Georgia",
                    ),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                ],
              ),
              Form(
                key: _regFormKey,
                child: Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: TextFormField(
                        decoration: InputDecoration(
                          hintText: "Username",
                        ),
                        onChanged: (value) {
                          username = value;
                        },
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: TextFormField(
                        onChanged: (value) {
                          password1 = value;
                        },
                        decoration: InputDecoration(
                          hintText: "Password",
                          suffixIcon: IconButton(
                            onPressed: toggleVisibility,
                            icon: Icon(isVisible
                                ? Icons.visibility_outlined
                                : Icons.visibility_off_outlined),
                          ),
                        ),
                        obscureText: !isVisible,
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: TextFormField(
                        onChanged: (value) {
                          password2 = value;
                        },
                        decoration: InputDecoration(
                          hintText: "Confirm Password",
                          suffixIcon: IconButton(
                            onPressed: toggleVisibility,
                            icon: Icon(isVisible
                                ? Icons.visibility_outlined
                                : Icons.visibility_off_outlined),
                          ),
                        ),
                        obscureText: !isVisible,
                      ),
                    ),
                    Row(
                      children: [
                        Radio<String>(
                            value: "Author",
                            groupValue: role,
                            onChanged: (value) {
                              setState(() {
                                role = value!;
                              });
                            }),
                        Text("Author")
                      ],
                    ),
                    Row(
                      children: [
                        Radio<String>(
                            value: "Customer",
                            groupValue: role,
                            onChanged: (value) {
                              setState(() {
                                role = value!;
                              });
                            }),
                        Text("Customer")
                      ],
                    ),
                    Row(
                      children: [
                        Radio<String>(
                            value: "Seller",
                            groupValue: role,
                            onChanged: (value) {
                              setState(() {
                                role = value!;
                              });
                            }),
                        Text("Seller")
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 50,
              ),
              Container(
                child: ElevatedButton(
                  onPressed: () async {
                    if (_regFormKey.currentState!.validate()) {
                      final response = await http.post(
                        Uri.parse(
                            "https://lindungipeduli.herokuapp.com/reg_apk/"),
                        headers: <String, String>{
                          "Content-Type": "application/json;charset=UTF-8",
                        },
                        body: jsonEncode(<String, String>{
                          'username': username,
                          'password1': password1,
                          'password2': password2,
                          'role': role,
                        }),
                      );
                      final resp = jsonDecode(response.body);
                      if (resp['status'] == 'success') {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LoginPage()));
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(content: Text('Register Failed')));
                      }
                    }
                  },
                  child: Text("REGISTER"),
                  style: ElevatedButton.styleFrom(
                    primary: Colors.green,
                    alignment: Alignment.center,
                    shape: StadiumBorder(),
                    padding: EdgeInsets.all(15),
                  ),
                ),
              ),
              SizedBox(
                height: 50,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Already Have an Account?"),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => LoginPage()));
                    },
                    child: Text(
                      "Login",
                      style: TextStyle(color: Colors.blue),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
