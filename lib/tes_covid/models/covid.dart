// To parse this JSON data, do
//
//     final tesCovid = tesCovidFromJson(jsonString);

import 'dart:convert';

TesCovid tesCovidFromJson(String str) => TesCovid.fromJson(json.decode(str));

String tesCovidToJson(TesCovid data) => json.encode(data.toJson());

class TesCovid {
  TesCovid({
    required this.covidTests,
  });

  List<CovidTest> covidTests;

  factory TesCovid.fromJson(Map<String, dynamic> json) => TesCovid(
    covidTests: List<CovidTest>.from(json["covidTests"].map((x) => CovidTest.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "covidTests": covidTests[0].toJson(),
  };
}

class CovidTest {
  CovidTest({
    required this.title,
    required this.type,
    this.testImage,
    required this.city,
    required this.price,
    required this.location,
    this.description,
    this.link,
    this.resultTime,
    this.schedule,
    this.phone,
    this.email,
  });

  String title;
  String type;
  String? testImage;
  String city;
  int price;
  String location;
  String? description;
  String? link;
  String? resultTime;
  String? schedule;
  String? phone;
  String? email;

  factory CovidTest.fromJson(Map<String, dynamic> json) => CovidTest(
    title: json["title"],
    type: json["type"],
    testImage: json["test_image"],
    city: json["city"],
    price: json["price"],
    location: json["location"],
    description: json["description"],
    link: json["link"],
    resultTime: json["result_time"],
    schedule: json["schedule"],
    phone: json["phone"],
    email: json["email"] == null ? null : json["email"],
  );

  Map<String, dynamic> toJson() => {
    "title": title,
    "type": type,
    "test_image": testImage,
    "city": city,
    "price": price,
    "location": location,
    "description": description,
    "link": link,
    "result_time": resultTime,
    "schedule": schedule,
    "phone": phone,
    "email": email == null ? null : email,
  };
}
