
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:uas_project/pengaturan_pengguna/login_page.dart';
import 'package:uas_project/tes_covid/screens/detail_tes_covid.dart';

class Dialog extends StatelessWidget {

  String title;
  String content;
  VoidCallback continueCallBack;

  Dialog(this.title, this.content, this.continueCallBack);
  TextStyle textStyle = TextStyle (color: Colors.black);

  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 6, sigmaY: 6),
        child:  AlertDialog(
          title: new Text(title,style: textStyle,),
          content: new Text(content, style: textStyle,),
          actions: <Widget>[
            TextButton(
              child: Text("Login"),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const LoginPage()));
              },
            ),
            TextButton(
              child: Text("Kembali"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ));
  }
}

_showDialog(BuildContext context, title) {

  VoidCallback continueCallBack = () => {
    Navigator.of(context).pop(),
    // code on continue comes here
  };
  Dialog alert = Dialog("Login", "Anda perlu login untuk melihat detail " + title,continueCallBack);


  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

Widget covidTestTemplate(covidTest, context, onGoBack) {
  return Card(
    shadowColor: Colors.black38,
    margin: const EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 8.0),
    child: Padding(
      padding: const EdgeInsets.all(15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(8.0),
              topRight: Radius.circular(8.0),
            ),
            child: Image.network(covidTest.testImage,
                width: 150, height: 160, fit: BoxFit.cover),
          ),
          SizedBox(height: 6.0),
          Text(
            covidTest.title,
            style: TextStyle(
              fontSize: 20.0,
              color: Colors.grey[800],
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 6.0),
          Text(
            covidTest.location,
            style: TextStyle(
              fontSize: 14.0,
              color: Colors.grey[500],
            ),
          ),
          SizedBox(height: 15.0),
          Text(
            "Rp" + covidTest.price.toString(),
            style: TextStyle(
              fontSize: 30.0,
              color: Colors.red[800],
              fontWeight: FontWeight.bold,
            ),
          ),
          OutlinedButton(
            style: TextButton.styleFrom(
              primary: Colors.blue,
              onSurface: Colors.red,
            ),
            // Within the `FirstRoute` widget
            onPressed: () {
              if(LoginPage.roles != ""){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => DetailPage(tescovid: covidTest)),
                ).then(onGoBack);
              } else {
                _showDialog(context, covidTest.title);
              }
            },
            child: Text('Lebih Lengkap'),
          )
        ],
      ),
    ),
  );
}