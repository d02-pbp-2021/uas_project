import 'package:flutter/material.dart';

// https://api.flutter.dev/flutter/material/FilterChip-class.html
class CityFilterEntry {
  const CityFilterEntry(this.name, this.value);
  final String name;
  final String value;
}

class CityFilter extends StatefulWidget {
  final ValueChanged<List<String>> update;
  const CityFilter({Key? key, required this.update}) : super(key: key);

  @override
  State createState() => CityFilterState(update: update);
}

class CityFilterState extends State<CityFilter> {
  late final ValueChanged<List<String>> update;
  CityFilterState({required this.update});

  final List<CityFilterEntry> _cast = <CityFilterEntry>[
    const CityFilterEntry('Jakarta', '1'),
    const CityFilterEntry('Bekasi', '2'),
    const CityFilterEntry('Bogor', '3'),
    const CityFilterEntry('Depok', '4'),
    const CityFilterEntry('Tangerang', '5'),
  ];
  final List<String> filters = <String>[];

  Iterable<Widget> get cityWidgets sync* {
    for (final CityFilterEntry category in _cast) {
      yield Padding(
        padding: const EdgeInsets.only(left: 8.0),
        child: FilterChip(
          label: Text(category.name,
            style: const TextStyle(
              color: Colors.white,
            ),),
          backgroundColor: const Color(0xFF9575cd),
          selected: filters.contains(category.name),
          onSelected: (bool value) {
            setState(() {
              if (value) {
                filters.add(category.name);
                update(filters);
              } else {
                filters.removeWhere((String name) {
                  return name == category.name;
                });
                update(filters);
              }
            });
          },
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Wrap(
            children: cityWidgets.toList(),
          ),
        ),
      ],
    );
  }
}