import 'package:flutter/material.dart';

// https://api.flutter.dev/flutter/material/FilterChip-class.html
class CategoryFilterEntry {
  const CategoryFilterEntry(this.name, this.value);
  final String name;
  final String value;
}

class CastFilter extends StatefulWidget {
  final ValueChanged<List<String>> update;
  const CastFilter({Key? key, required this.update}) : super(key: key);

  @override
  State createState() => CastFilterState(update: update);
}

class CastFilterState extends State<CastFilter> {
  late final ValueChanged<List<String>> update;
  CastFilterState({required this.update});

  final List<CategoryFilterEntry> _cast = <CategoryFilterEntry>[
    const CategoryFilterEntry('Rapid', '1'),
    const CategoryFilterEntry('Swab Antigen', '2'),
    const CategoryFilterEntry('PCR', '3'),
    const CategoryFilterEntry('PCR Drive Thru', '4'),
    const CategoryFilterEntry('Tes Serologi', '5'),
  ];
  final List<String> filters = <String>[];

  List<String> getFilterData(){
    return filters;
  }

  Iterable<Widget> get categoryWidgets sync* {
    for (final CategoryFilterEntry category in _cast) {
      yield Padding(
        padding: const EdgeInsets.only(left: 8.0),
        child: FilterChip(
          label: Text(category.name,
            style: const TextStyle(
              color: Colors.white,
            ),),
          backgroundColor: const Color(0xFFff8a65),
          selected: filters.contains(category.name),
          onSelected: (bool value) {
            setState(() {
              if (value) {
                filters.add(category.name);
                update(filters);

              } else {
                filters.removeWhere((String name) {
                  return name == category.name;
                });
                update(filters);
              }
            });
          },
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Wrap(
            children: categoryWidgets.toList(),
          ),
        ),
      ],
    );
  }
}