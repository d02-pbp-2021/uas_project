import 'dart:convert';
import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:uas_project/pengaturan_pengguna/login_page.dart';
import 'package:uas_project/tes_covid/screens/tes_covid_screen.dart';
import 'package:uas_project/tes_covid/models/covid.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

// https://stackoverflow.com/questions/53844052/how-to-make-an-alertdialog-in-flutter
class BlurryDialog extends StatelessWidget {

  String title;
  String content;
  VoidCallback continueCallBack;

  BlurryDialog(this.title, this.content, this.continueCallBack);
  TextStyle textStyle = TextStyle (color: Colors.black);

  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 6, sigmaY: 6),
        child:  AlertDialog(
          title: new Text(title,style: textStyle,),
          content: new Text(content, style: textStyle,),
          actions: <Widget>[
            TextButton(
              child: Text("Hapus"),
              onPressed: () {
                continueCallBack();
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text("Kembali"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ));
  }
}

class DetailPage extends StatelessWidget {
  final CovidTest tescovid;
  const DetailPage({Key? key, required this.tescovid}) : super(key: key);

  _showDialog(BuildContext context) {

    VoidCallback continueCallBack = () => {
      Navigator.of(context).pop(),
      deleteTest(tescovid),
      // code on continue comes here
    };
    BlurryDialog  alert = BlurryDialog("Hapus","Apakah kamu yakin untuk menghapus " + tescovid.title + "?",continueCallBack);


    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Future<CovidTest> deleteTest(CovidTest covidTest) async {
    final http.Response response = await http.delete(

      Uri.parse('https://lindungipeduli.herokuapp.com/covid-test/kirim-data'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: json.encode(covidTest.toJson()),
    );
    print(json.encode(covidTest.toJson()));
    print(response.body);

    if (response.statusCode == 204) {
      return covidTest;
    } else {
      // If the server did not return a "200 OK response",
      // then throw an exception.
      throw Exception('Failed to delete album.');
    }
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: Text("Tes Covid"),
      ),
      body: Center(
          child: Padding (
              padding: EdgeInsets.all(20),
              child: ListView(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(8.0),
                      topRight: Radius.circular(8.0),
                    ),
                    child: Image.network(tescovid.testImage ?? "https://fajar.co.id/wp-content/uploads/img/no-image.jpg",
                        width: 150, height: 200, fit: BoxFit.cover),
                  ),
                  SizedBox(height: 20),
                  SizedBox(
                    height: 30,
                    width: 30,
                    child: Center(
                      child: Text(
                        tescovid.title,
                        style: TextStyle(
                          fontSize: 25.0,
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 50,
                    width: 30,
                    child: Center(
                      child: Text(
                        "Rp" + tescovid.price.toString(),
                        style: TextStyle(
                          fontSize: 40.0,
                          color: Colors.red[800],
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 20),
                  LoginPage.roles == "Admin" ? ElevatedButton(
                    onPressed: () { _showDialog(context); },
                    child: Text("Hapus", style: const TextStyle(fontSize: 20)),
                    style: ElevatedButton.styleFrom(
                      primary: Colors.red[800],
                      padding: EdgeInsets.symmetric(horizontal: 30, vertical: 16),
                    ),
                  ) : SizedBox(),
                  const SizedBox(height: 20),
                  Container(
                    child: Table(
                      defaultColumnWidth: FixedColumnWidth(150.0),
                      children: [
                        TableRow(children: [
                          Column(children: [
                            Container(
                              width: 600,
                              child: Column(
                                  children: [
                                    SizedBox(height: 16),
                                    const Text(
                                      "Kategori",
                                      style: TextStyle(
                                        fontSize: 20.0,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    SizedBox(height: 10),
                                    Text(
                                      tescovid.type,
                                      style: const TextStyle(
                                        fontSize: 16.0,
                                        color: Colors.black,
                                      ),
                                    ),
                                    SizedBox(height: 16),
                                  ]
                              ),
                              decoration: BoxDecoration(
                                color: Colors.grey[200],
                              ),
                            ),
                          ]
                          ),
                        ]),
                        TableRow( children: [
                          Column(children: [
                            Container(
                              width: 600,
                              child: Column(
                                  children: [
                                    SizedBox(height: 16),
                                    Text(
                                      "Hasil Tes",
                                      style: TextStyle(
                                        fontSize: 20.0,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    SizedBox(height: 10),
                                    Text(
                                      tescovid.resultTime ?? "-",
                                      style: const TextStyle(
                                        fontSize: 16.0,
                                        color: Colors.black,
                                      ),
                                    ),
                                    SizedBox(height: 16),
                                  ]
                              ),
                              decoration: BoxDecoration(
                                color: Colors.white,
                              ),
                            ),
                          ]
                          ),
                        ]),
                        TableRow( children: [
                          Column(children: [
                            Container(
                              width: 600,
                              child: Column(
                                  children: [
                                    SizedBox(height: 16),
                                    const Text(
                                      "Lokasi",
                                      style: TextStyle(
                                        fontSize: 20.0,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    SizedBox(height: 10),
                                    Text(
                                      tescovid.location,
                                      style: const TextStyle(
                                        fontSize: 16.0,
                                        color: Colors.black,
                                      ),
                                    ),
                                    SizedBox(height: 16),
                                  ]
                              ),
                              decoration: BoxDecoration(
                                color: Colors.grey[200],
                              ),
                            ),
                          ]
                          ),
                        ]),
                        TableRow( children: [
                          Column(children: [
                            Container(
                              width: 600,
                              child: Column(
                                  children: [
                                    SizedBox(height: 16),
                                    Text(
                                      "Jadwal",
                                      style: TextStyle(
                                        fontSize: 20.0,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    SizedBox(height: 10),
                                    Text(
                                      tescovid.schedule ?? "-",
                                      style: const TextStyle(
                                        fontSize: 16.0,
                                        color: Colors.black,
                                      ),
                                    ),
                                    SizedBox(height: 16),
                                  ]
                              ),
                              decoration: BoxDecoration(
                                color: Colors.white,
                              ),
                            ),
                          ]
                          ),
                        ]),
                        TableRow( children: [
                          Column(children: [
                            Container(
                              width: 600,
                              child: Column(
                                  children: [
                                    SizedBox(height: 16),
                                    Text(
                                      "Deskripsi",
                                      style: TextStyle(
                                        fontSize: 20.0,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    SizedBox(height: 10),
                                    Text(
                                      tescovid.description ?? "Tidak ada deskripsi",
                                      style: const TextStyle(
                                        fontSize: 16.0,
                                        color: Colors.black,
                                      ),
                                    ),
                                    SizedBox(height: 16),
                                  ]
                              ),
                              decoration: BoxDecoration(
                                color: Colors.grey[200],
                              ),
                            ),
                          ]
                          ),
                        ]),
                        TableRow( children: [
                          Column(children: [
                            Container(
                              width: 600,
                              child: Column(
                                  children: [
                                    SizedBox(height: 16),
                                    Text(
                                      "No. Telepon",
                                      style: TextStyle(
                                        fontSize: 20.0,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    SizedBox(height: 10),
                                    Text(
                                      tescovid.phone ?? "-",
                                      style: const TextStyle(
                                        fontSize: 16.0,
                                        color: Colors.black,
                                      ),
                                    ),
                                    SizedBox(height: 16),
                                  ]
                              ),
                              decoration: BoxDecoration(
                                color: Colors.white,
                              ),
                            ),
                          ]
                          ),
                        ]),
                        TableRow( children: [
                          Column(children: [
                            Container(
                              width: 600,
                              height: 80,
                              child: Column(
                                  children: [
                                    SizedBox(height: 12),
                                    Text(
                                      "Email",
                                      style: TextStyle(
                                        fontSize: 20.0,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    SizedBox(height: 10),
                                    Text(
                                      tescovid.email ?? "-",
                                      style: const TextStyle(
                                        fontSize: 16.0,
                                        color: Colors.black,
                                      ),
                                    ),
                                    SizedBox(height: 12),
                                  ]
                              ),
                              decoration: BoxDecoration(
                                color: Colors.grey[200],
                              ),
                            ),
                          ]
                          ),
                        ]),
                      ],
                    ),
                  ),
                  SizedBox(height: 60),
                ],
              )
          )
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          _launchURL(tescovid.link ?? "https://lindungipeduli.herokuapp.com/covid-test");
        },
        icon: Icon(Icons.how_to_reg),
        label: Text("Daftar Sekarang"),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16.0))),
        backgroundColor: Colors.blueGrey,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}