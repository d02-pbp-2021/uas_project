import 'package:flutter/material.dart';
import 'package:uas_project/pengaturan_pengguna/login_page.dart';
import 'package:uas_project/tes_covid/widgets/covid_filter_tipe.dart';
import 'package:uas_project/tes_covid/screens/tambah_tes_covid.dart';
import 'package:uas_project/tes_covid/widgets/covid_card.dart';
import 'package:uas_project/widgets/drawer.dart';
import 'package:http/http.dart' as http;
import 'package:uas_project/tes_covid/models/covid.dart';
import 'package:uas_project/tes_covid/widgets/covid_filter_kota.dart';
import 'dart:async';

var _controller = TextEditingController();

class TesCovidScreen extends StatelessWidget {
  const TesCovidScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // https://flutter.dev/docs/development/ui/assets-and-images
        theme: ThemeData(
          // Define the default brightness and colors.
          brightness: Brightness.light,
          primaryColor: Colors.lightBlue[800],

          // Define the default font family.
          fontFamily: 'Fredoka One',

          // Define the default `TextTheme`. Use this to specify the default
          // text styling for headlines, titles, bodies of text, and more.
          textTheme: const TextTheme(
            headline6: TextStyle(fontSize: 24.0),
          ),
        ),
        home: Scaffold(
          body: Container(
            child: MyCardWidget(),
          ),
        )
    );
  }
}

class MyCardWidget extends StatefulWidget {

  const MyCardWidget({Key? key}) : super(key: key);

  @override
  MyCardWidgetState createState() {
    return MyCardWidgetState();
  }
}


class MyCardWidgetState extends State<MyCardWidget> {
  List<String> filterType = [];
  List<String> filterCity = [];

  Future<List<CovidTest>> _fetchData(String query, bool isFilter) async {
    var url = Uri.parse(
        'https://lindungipeduli.herokuapp.com/covid-test/kirim-data');
    var response =
    await http.get(url, headers: {"Access-Control_Allow_Origin": "*"});

    //var data = jsonDecode(response.body);
    List<CovidTest> tesCovids = tesCovidFromJson(response.body).covidTests;
    if (isFilter){
      tesCovids = tesCovids.where((tes) {
        bool masukFilterType = filterType.length == 0;
        bool masukFilterCity = filterCity.length == 0;
        for (String i in filterType){
          if (i == tes.type){masukFilterType = true;}
        }
        for (String i in filterCity){
          if (i == tes.city){masukFilterCity = true;}
        }
        return masukFilterType && masukFilterCity;
      }).toList();
    } else {
      tesCovids = tesCovids.where((tes) {
        final titleLower = tes.title.toLowerCase();
        final locationLower = tes.location.toLowerCase();
        final queryLower = query.toLowerCase();

        return titleLower.contains(queryLower) || locationLower.contains(queryLower);
      }).toList();
    }
    return tesCovids;
  }

  final ScrollController _firstController = ScrollController();
  String query = "";
  List<CovidTest> _tesCovidList = [];
  Timer? debouncer;

  // Sumber: https://github.com/JohannesMilke/filter_listview_example
  @override
  void initState() {
    if (firstData){
      filterTest();
      _updateFirstData();
    }
    super.initState();
  }

  @override
  void dispose() {
    debouncer?.cancel();
    super.dispose();
  }

  void debounce(
      VoidCallback callback, {
        Duration duration = const Duration(milliseconds: 1000),
      }) {
    if (debouncer != null) {
      debouncer!.cancel();
    }

    debouncer = Timer(duration, callback);
  }

  Future searchTest(String query) async => debounce(() async {
    final tesCovids = await _fetchData(query, false);

    setState(() {
      this.query = query;
      _tesCovidList = tesCovids;
    });
  });

  void _updateType(List<String> filters) {
    setState(() => filterType = filters);
    print('arr tipe $filterType');
    filterTest();
  }
  void _updateCity(List<String> filters) {
    setState(() => filterCity = filters);
    print('arr kota $filterCity');
    filterTest();
  }

  FutureOr onGoBack(dynamic value) {
    filterTest();
  }

  Future filterTest() async => debounce(() async {
    if (!mounted) return;
    print("masuuuk");
    final tesCovids = await _fetchData("", true);

    setState(() {
      _tesCovidList = tesCovids;
    });
  });

  void _updateFirstData(){
    setState(() => firstData = false);
  }

  bool firstData = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Container(
            width: double.infinity,
            height: 40,
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(5)),
            child: Center(
              child: TextField(
                controller: _controller,
                onChanged: (text) {
                  print('First text field: $text');
                  searchTest(text);
                },
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.search, color: Colors.blueGrey),
                    suffixIcon: IconButton(
                        icon: Icon(Icons.clear, color: Colors.blueGrey),
                        onPressed: () {
                          _controller.clear();
                          searchTest("");
                        }
                    ),
                    hintText: 'Cari Tes Covid..',
                    border: InputBorder.none),
              ),
            ),
          ),
          backgroundColor: Colors.blueGrey,
        ),
        backgroundColor: Colors.white,
        //https://api.flutter.dev/flutter/material/FloatingActionButton-class.html
        floatingActionButton: LoginPage.roles == "Admin" ? FloatingActionButton(
          onPressed: () =>
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => MyCustomForm()),
              ).then(onGoBack),
          child: const Icon(Icons.add),
          backgroundColor: Colors.blueGrey,
        ) : null,
        //https://material.io/components/navigation-drawer/flutter#standard-navigation-drawer
        drawer: const MyDrawer(),
        body: Column(
            children: <Widget>[
              const SizedBox(height: 10),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 140),
                child: Row(
                  children: [
                    const Text( "Tes", style: TextStyle(
                        fontSize: 30,
                        color: Colors.amber,
                        fontWeight: FontWeight.w700),
                    ),
                    const Text( "Covid", style: TextStyle(
                        fontSize: 30,
                        color: Colors.blue,
                        fontWeight: FontWeight.w700),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: CastFilter(update: _updateType),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: CityFilter(update: _updateCity),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: FutureBuilder<List<CovidTest>>(
                    future: _fetchData(query, true),
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return Scrollbar(
                            isAlwaysShown: true,
                            controller: _firstController,
                            child: ListView.builder(
                                controller: _firstController,
                                itemCount: _tesCovidList.length,
                                itemBuilder: (context, index) {
                                  return  covidTestTemplate(_tesCovidList[index], context, onGoBack);
                                })
                        );
                      } else if (snapshot.hasError) {
                        return Text('${snapshot.error}');
                      }

                      // By default, show a loading spinner.
                      return Center(child: Column(
                        children: const [
                          SizedBox(height: 100),
                          Text("Sedang mengambil data.."),
                          SizedBox (height: 20),
                          SizedBox( width: 30, height: 30, child: CircularProgressIndicator()),
                        ],
                      ),
                      );
                    },
                  ),
                ),)
            ]
        )
    );
  }
}