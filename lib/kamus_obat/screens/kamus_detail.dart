import 'package:flutter/material.dart';
import 'package:uas_project/kamus_obat/models/kamus_models.dart';
import 'package:uas_project/pengaturan_pengguna/login_page.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class KamusDetail extends StatelessWidget {
  const KamusDetail({Key? key, required this.data}) : super(key: key);
  final KamusModels data;

  // REFERENSI: https://stackoverflow.com/questions/53844052/how-to-make-an-alertdialog-in-flutter
  showAlertDialog(BuildContext context) {

    // set up the buttons
    Widget cancelButton = TextButton(
      child: Text("Cancel"),
      onPressed:  () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = TextButton(
      child: Text("Continue"),
      onPressed:  () async {

        String deleteURL = "https://lindungipeduli.herokuapp.com/kamus-obat/delete-post/";
        var url = Uri.parse(deleteURL);
        final response = await http.post(url,
            body: json.encode(
                {"pk" : data.pk}
            ));

        Navigator.pop(context);
        Navigator.pop(context);

      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("AlertDialog"),
      content: Text("Are you sure you want to delete ${data.fields!.name}?"),
      actions: [
        cancelButton,
        continueButton,
      ],

    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final double width = size.width;
    final double height = size.height;

    return Scaffold(

      appBar: AppBar(
          title: const Text(
            "Kamus Detail",
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        backgroundColor: Colors.blueGrey,
          centerTitle: true,
        ),
        body: ListView(
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: width / 10),
              child: Container(
                alignment: Alignment.bottomLeft,
                padding: EdgeInsets.only(bottom: 20.0),
                child: Text('${data.fields!.name}',
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 24.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              width: width,
              height: height / 3,
              decoration: BoxDecoration(
                image: DecorationImage(
                  colorFilter: ColorFilter.mode(
                    Colors.black.withOpacity(0.25),
                    BlendMode.srcOver,
                  ),
                  fit: BoxFit.cover,
                  image: NetworkImage('${data.fields!.image}',),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: width / 20, top: height / 40),
              child: const Text(
                "Description",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                  left: width / 10, top: height / 40, right: width / 10),
              child: Text('${data.fields!.description}',
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: width / 20, top: height / 40),
              child: const Text(
                "Effect",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                  left: width / 10, top: height / 40, right: width / 10),
              child: Text('${data.fields!.effect}',
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: width / 20, top: height / 40),
              child: const Text(
                "Composition",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                  left: width / 10, top: height / 40, right: width / 10),
              child: Text('${data.fields!.ingredients}',
              ),
            ),

            /*
            * Delete
            * Inspired By: Judah Ariesaka
            * */
            Column(
              children: [
                 Container(
                  margin: EdgeInsets.all(10),
                  child: LoginPage.roles == "Admin" ? RaisedButton(
                      color: Colors.red[800],
                      onPressed: () {
                        showAlertDialog(context);
                      },

                      child: const Text("Delete", style: TextStyle(
                        color: Colors.white,
                      ),)
                  ) : null,
                )
              ],
            )
          ],
        ),

    );
  }
}
