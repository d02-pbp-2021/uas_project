import 'dart:async';
import 'package:flutter/material.dart';
import 'package:uas_project/kamus_obat/models/fetch_kamus.dart';
import 'package:uas_project/kamus_obat/screens/kamus_form.dart';
import 'package:uas_project/kamus_obat/models/kamus_models.dart';
import 'package:uas_project/kamus_obat/screens/kamus_detail.dart';
import 'package:uas_project/widgets/drawer.dart';
import 'package:uas_project/pengaturan_pengguna/login_page.dart';


var _controller = TextEditingController();


/* REFERENSI
* https://blog.waldo.io/flutter-card/
*
* INSPIRED BY:
* Search: Melati Eka Putri
* Fetch Data: Fadhli Galzaba
* */
class KamusHomepage extends StatefulWidget {
  const KamusHomepage({Key? key}) : super(key: key);

  @override
  _KamusHomepageState createState() => _KamusHomepageState();
}

class _KamusHomepageState extends State<KamusHomepage> {
  final FetchKamus kamus = FetchKamus();

  String query = "";
  List<KamusModels> data = [];
  Icon actionIcon = new Icon(Icons.search);
  Widget appBarTitle = new Text("Kamus Obat");

  @override
  void initState() {
    searchKamus("");

    super.initState();
  }




  Future searchKamus(String query) async{
    if (!mounted) return;
    final result = await kamus.getKamus(query);

    setState(() {
      this.query = query;
      data = result;
    });
  }

  // REFERENSI: https://stackoverflow.com/questions/62608782/how-to-make-keyboard-appear-when-press-the-search-icon-and-select-at-the-text-fi
  FocusNode focusNode = FocusNode();



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: appBarTitle,
        backgroundColor: Colors.blueGrey,
        actions: [
          // REFERENSI: https://medium.com/@info_67212/how-to-perform-a-search-in-flutter-app-1365bd317a0c
          IconButton(icon: actionIcon,onPressed:(){
            setState(() {
              focusNode.requestFocus();
              if ( actionIcon.icon == Icons.search){
                actionIcon = new Icon(Icons.close);

                appBarTitle = TextField(
                  focusNode: focusNode,
                  controller: _controller,
                  onChanged: (text) {
                    setState(() {
                      searchKamus(text);
                    });

                  },

                  style: TextStyle(
                    color: Colors.white,

                  ),
                  decoration: InputDecoration(
                      prefixIcon: Icon(Icons.search, color: Colors.blueGrey),
                      hintText: "Search...",
                      hintStyle: TextStyle(color: Colors.white)
                  ),
                );}
              else {
                actionIcon = Icon(Icons.search);
                appBarTitle = Text("Kamus Obat");
                _controller.clear();
                setState(() {
                  searchKamus("");
                });

              }

            });
          } ,),
        ],

      ),
      drawer: MyDrawer(),

      // form
      floatingActionButton: LoginPage.roles == "Admin" ? FloatingActionButton(
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return KamusForm();
          }));
        },
        backgroundColor: Colors.blue,
        child: Icon(Icons.add),
      ) : null,
      body:Column(
        children: [
          SizedBox(
            height: 65,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const <Widget>[
                Text( "Kamus", style: TextStyle(
                    fontSize: 30,
                    color: Colors.amber,
                    fontWeight: FontWeight.w700),
                ),
                Text( "Obat", style: TextStyle(
                    fontSize: 30,
                    color: Colors.blue,
                    fontWeight: FontWeight.w700),
                ),
              ],
            ),
          ),
          Expanded(
              child: Padding(
                padding: const EdgeInsets.all(15),
                child: FutureBuilder<List<KamusModels>> (
                  future: kamus.getKamus(query),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return const Center(child: CircularProgressIndicator());
                    }
                    return Center(
                      child: ListView.builder(
                        itemCount: data.length,
                        scrollDirection: Axis.vertical,
                        itemBuilder: (context, index) => GestureDetector(
                            onTap: () async {
                              Navigator.of(context).push(
                                  MaterialPageRoute(builder:
                                      (context) => KamusDetail(data: data[index])));
                            },
                            child: Card(
                              margin: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                              elevation: 4.0,
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    width: MediaQuery.of(context).size.width,
                                    height: 150,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        fit: BoxFit.fitWidth,
                                        image: NetworkImage('${data[index].fields!.image}'),
                                      ),
                                    ),
                                  ),
                                  ListTile(
                                    title: Text('${data[index].fields!.name}',
                                        style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold,)
                                    ),
                                    subtitle: Text('${data[index].fields!.description}',
                                        style: TextStyle( fontSize: 16,)
                                    ),
                                  ),
                                ],
                              ),
                            )
                        ),
                      ),
                    );
                  },
                ),
              )
          )
        ],
      )
    );
  }
}


