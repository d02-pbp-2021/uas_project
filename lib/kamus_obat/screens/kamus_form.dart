import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

/*
* REFERENSI:
* https://docs.flutter.dev/cookbook/forms/text-input
* https://docs.flutter.dev/cookbook/forms/text-input
*
* INSPIRED BY: Fadhli Gazalba
* */
class KamusForm extends StatefulWidget {
  const KamusForm({Key? key}) : super(key: key);

  @override
  _KamusFormState createState() => _KamusFormState();
}

class _KamusFormState extends State<KamusForm> {
  final _formKey = GlobalKey<FormState>();
  String? image = "";
  String? name = "";
  String? description = "";
  String? effect = "";
  String? ingredients = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(
         title: Text("Form Kamus Obat"),
         backgroundColor: Colors.blueGrey,
       ),
      body: Center(
        child: ListView(
          children: <Widget>[

            Container(
              height: 100,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Tambah",
                    style: TextStyle(
                        fontSize: 30,
                        color: Colors.amber,
                        fontWeight: FontWeight.w700),
                  ),

                  Text(
                    "KamusObat",
                    style: TextStyle(
                        fontSize: 30,
                        color: Colors.blue,
                        fontWeight: FontWeight.w700),
                  ),
                ],
              ),
            ),


              Form(
                key: _formKey,
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                      child: TextFormField(
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter some text';
                          }
                          return null;
                        },
                        onChanged: (value) {
                          setState(() {
                            image = value;
                          });
                        },
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: 'eg: https//:panadol_extra.jpg',
                          labelText: 'Link Foto Obat',
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                      child: TextFormField(
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                        onChanged: (value) {
                          setState(() {
                            name = value;
                          });
                        },
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: 'eg: Panadol Extra',
                          labelText: 'Nama Obat',
                        ),
                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                      child: TextFormField(
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter some text';
                          }
                          return null;
                        },
                        onChanged: (value) {
                          setState(() {
                            description = value;
                          });
                        },
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: 'eg: Obat pereda pusing',
                          labelText: 'Deskripsi Obat',
                        ),
                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                      child: TextFormField(
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter some text';
                          }
                          return null;
                        },
                        onChanged: (value) {
                          setState(() {
                            effect = value;
                          });
                        },
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: 'eg: Mual-mual',
                          labelText: 'Efek Samping Obat',
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                      child: TextFormField(
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter some text';
                          }
                          return null;
                        },
                        onChanged: (value) {
                          setState(() {
                            ingredients = value;
                          });
                        },
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: 'eg: Caffeine 500mg',
                          labelText: 'Komposisi Obat',
                        ),
                      ),
                    ),

                    Container(
                      margin: EdgeInsets.all(10),
                      child: RaisedButton(
                        color: Colors.amber,
                        onPressed: () async {
                          String postUrl = "https://lindungipeduli.herokuapp.com/kamus-obat/post/";
                          var url = Uri.parse(postUrl);
                          // Validate returns true if the form is valid, or false otherwise.
                          if (_formKey.currentState!.validate()) {
                            // If the form is valid, display a snackbar. In the real world,
                            // you'd often call a server or save the information in a database.
                            final response = await http.post(url,
                                body: json.encode(
                                  {
                                    'image' : image,
                                    'name' : name,
                                    'description' : description,
                                    'effect' : effect,
                                    'ingredients' : ingredients,
                                  }
                                ));
                            _formKey.currentState?.reset();
                            setState((){});
                            Navigator.pop(context);
                          }else {
                            ScaffoldMessenger.of(context).showSnackBar(
                              const SnackBar(content: Text('Your data failed to be saved!')),
                            );
                          }
                        },
                        child: const Text('Submit',  style: TextStyle(
                          color: Colors.white,
                        ),),
                      ),
                    ),
                  ],
                ),
              )

            ],
          ),
      ),
    );
  }
}
