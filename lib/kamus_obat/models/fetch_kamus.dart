import 'dart:convert';

import 'package:http/http.dart' as http;
import 'kamus_models.dart';

class FetchKamus {
  var data = [];
  List<KamusModels> result = [];

  String fecthUrl = 'https://lindungipeduli.herokuapp.com/kamus-obat/json/';

  Future<List<KamusModels>> getKamus(String query) async {
    var url = Uri.parse(fecthUrl);
    var response = await http.get(url);

    try {
      if (response.statusCode == 200) {
        data = json.decode(response.body);
        result = data.map((e) => KamusModels.fromJson(e)).toList();
        result = result.where((q) {
          final queryLower = query.toLowerCase();
          final titleLower = '${q.fields!.name}'.toLowerCase();
          return titleLower.contains(queryLower);
        }).toList();

      } else {
        print("api error");
      }
    } on Exception catch (e) {
      print('error: $e');
    }

    return result;
  }
}