// GENERATOR: https://javiercbk.github.io/json_to_dart/

class KamusModels {
  String? model;
  int? pk;
  Fields? fields;

  KamusModels({this.model, this.pk, this.fields});

  KamusModels.fromJson(Map<String, dynamic> json) {
    model = json['model'];
    pk = json['pk'];
    fields =
    json['fields'] != null ? new Fields.fromJson(json['fields']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['model'] = this.model;
    data['pk'] = this.pk;
    if (this.fields != null) {
      data['fields'] = this.fields!.toJson();
    }
    return data;
  }
}

class Fields {
  String? image;
  String? name;
  String? description;
  String? effect;
  String? ingredients;

  Fields(
      {this.image, this.name, this.description, this.effect, this.ingredients});

  Fields.fromJson(Map<String, dynamic> json) {
    image = json['image'];
    name = json['name'];
    description = json['description'];
    effect = json['effect'];
    ingredients = json['ingredients'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['image'] = this.image;
    data['name'] = this.name;
    data['description'] = this.description;
    data['effect'] = this.effect;
    data['ingredients'] = this.ingredients;
    return data;
  }
}