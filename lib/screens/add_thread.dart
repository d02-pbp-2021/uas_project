import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:uas_project/pengaturan_pengguna/register_page.dart';
import 'package:uas_project/pengaturan_pengguna/login_page.dart';

import '../widgets/drawer.dart';

class AddThread extends StatefulWidget {
    const AddThread({Key? key}) : super(key: key);

    @override
    State<AddThread> createState() => _AddThreadState();
}

class _AddThreadState extends State<AddThread> {
    final _formKey = GlobalKey<FormState>();
    
    String _topic = "";
    String _content = "";
    String _name = "Anonymous";
    
    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                title: Container(
                    child: Image.asset('assets/images/logokotak.png',
                    height: 56.0),
                ),
                backgroundColor: Colors.blueGrey,
            ),
            drawer: MyDrawer(),
            body: Form(
                key: _formKey,
                child: Column(
                    children: <Widget>[
                        Padding (
                            padding: const EdgeInsets.all(5.0),
                            child: TextFormField(
                                onChanged: (String value) {
                                    _topic = value;
                                },
                                decoration: const InputDecoration(
                                    hintText: 'What are you writing about?',
                                    labelText: 'Topic',
                                ),
                                onSaved: (String? value) {
                                    print("Apa tu man");
                                },
                                validator: (String? value) {
                                    if (value == null || value.isEmpty) {
                                        return 'Are you even trying? o_o';
                                    }
                                    return null;
                                },

                            ),
                        ),
                        Padding (
                            padding: const EdgeInsets.all(5.0),
                            child: TextFormField(
                                onChanged: (String value) {
                                    _content = value;
                                },
                                decoration: const InputDecoration(
                                    hintText: 'Brief explanation of your problem',
                                    labelText: 'Argument',
                                ),
                                onSaved: (String? value) {
                                    print("Apa tu man");
                                },
                                validator: (String? value) {
                                    if (value == null || value.isEmpty) {
                                        return 'Are you even trying? o_o';
                                    }
                                    return null;
                                },
                            ),
                        ),
                        Padding (
                            padding: const EdgeInsets.all(5.0),
                            child: TextFormField(
                                onChanged: (String value) {
                                    _name = value;
                                    if (value == "") {
                                        _name = "Anonymous";
                                    }
                                },
                                decoration: const InputDecoration(
                                    hintText: 'Leave this field empty to post as anonymous',
                                    labelText: 'Name',
                                ),
                                onSaved: (String? value) {
                                    print("Apa tu man");
                                },
                                validator: (String? value) {
                                    return null;
                                },
                            ),
                        ),
                        Padding (
                            padding: const EdgeInsets.all(5.0),
                            child: ElevatedButton(
                                onPressed: () {
                                    if (_formKey.currentState!.validate()) {
                                        if (LoginPage.roles != "") {
                                            submit();
                                            ScaffoldMessenger.of(context).showSnackBar(
                                                const SnackBar(content: Text('Processing Data')),
                                            );
                                        } else {
                                            Navigator.push(context, MaterialPageRoute(builder: (context) => RegisterPage()));
                                        }
                                    } else {
                                        print('not valid');
                                    }
                                },
                                child: const Text('Submit'),
                            ),
                        ),
                    ],
                ),
            ),
        );
    }

    void submit() async {
        print(_name);
        print(_content);
        print(_topic);

        var response = await http.post(
            Uri.parse('https://lindungipeduli.herokuapp.com/test/'),
            headers: {
                'Content-Type': 'application/json; charset=UTF-8',
            },
            body: jsonEncode(<String, String>{
                "name": _name,
                "content": _content,
                "topic" : _topic,
            }),
        );
        print(response.body);
    }
}
