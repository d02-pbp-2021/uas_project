import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'dart:convert';

import '../widgets/drawer.dart';
import '../widgets/thread_item.dart';
import '../models/thread.dart';

class HomePage extends StatefulWidget {
    const HomePage({Key? key}) : super(key: key);

    @override
    State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ScrollController _scrollController = ScrollController();
  List<Thread> _list = [];

  @override
  void initState() {
    Thread.getThread(_list.length.toString()).then((threads) {
      for (int i = 0; i < threads.length; i++) {
        setState(() {
          _list.add(threads[i]);
        });
      }
    });
    for (int i = 0; i < _list.length; i++) {
      print("TOPIC          " + _list[i].topic);
    }
    _scrollController.addListener((){
      if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        print("End of Page");
        Thread.getThread(_list.length.toString()).then((threads) {
          for (int i = 0; i < threads.length; i++) {
            setState(() {
              _list.add(threads[i]);
            });
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_list.length == 0) {
      return Scaffold(
        appBar: AppBar(
          title: Text("Please Wait..."),
          backgroundColor: Colors.blueGrey,
        ),
        body: Center(
          child: CupertinoActivityIndicator(
            radius: 50,
          ),
        ),
      );
    } else {
      return Scaffold(
        backgroundColor: Colors.grey[50],
        appBar: AppBar(
          title: Container(
            child: Row(
              children: [
                SizedBox(width: 30),
                Image.asset('assets/images/logokotak.png',
                    height: 56.0),
                SizedBox(width: 10),
                Text("LindungiPeduli", style: TextStyle(color: Colors.white),)
              ]
            )
          ),
          backgroundColor: Colors.blueGrey,
        ),
        drawer: MyDrawer(),
        body: ListView.builder(
          controller: _scrollController,
          itemBuilder: (context, i) {
            if (i == _list.length) {
              return CupertinoActivityIndicator();
            }
            return ListTile(
              title: ThreadItem(
                _list[i].topic,
                _list[i].slug,
                _list[i].name,
                getDate(_list[i].date_created),
                _list[i].content,
              ),
            );
          },
          itemCount: _list.length,
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Thread.getThread(_list.length.toString()).then((threads) {
              for (int i = 0; i < threads.length; i++) {
                setState(() {
                  _list.add(threads[i]);
                });
              }
            });
          },
          tooltip: 'Load More Threads',
          child: const Icon(Icons.more_horiz),
        ),
      );
    }
  }

  final List<String> MONTHS= [ "DUMMY",
    "January", "February", "March", "April",
    "May", "June", "July", "August",
    "September", "October", "November", "December"
  ];

  String getDate(String date) {
    String year = date.substring(0, 4);
    String month = date.substring(5, 7);
    String day = date.substring(8, 10);
    String hour = date.substring(11, 13);
    String minute = date.substring(14, 16);
    String time12 = convertTimeConvention(hour);
    
    hour = (int.parse(hour) % 12).toString();

    return
      day + " " + MONTHS[int.parse(month)] +
      " " + year + ", " + hour.padLeft(2, "0") + ":" + minute +
      " " + time12;
  }

  String convertTimeConvention(String hour) {
    int time = int.parse(hour);
    if (time >= 12) return "PM";
    return "AM";
  }
}
