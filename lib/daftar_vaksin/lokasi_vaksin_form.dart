import 'dart:convert';
import 'package:select_form_field/select_form_field.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:uas_project/daftar_vaksin/lokasi_vaksin.dart';

// Source: https://www.youtube.com/watch?v=ws-NL5_KEyc
// https://docs.flutter.dev/cookbook/networking/send-data
// https://docs.flutter.dev/cookbook/forms/validation

class FormLokasiVaksin extends StatefulWidget {
  const FormLokasiVaksin({Key? key}) : super(key: key);

  @override
  FormLokasiVaksinState createState() {
    return FormLokasiVaksinState();
  }
}

class FormLokasiVaksinState extends State<FormLokasiVaksin> {
  final _formKey = GlobalKey<FormState>();

  final List<Map<String, dynamic>> pilihanJenis = [
    {'label': 'Pfizer', 'value': 'Pfizer'}, {'label': 'Sinovac','value': 'Sinovac'},
    {'label': 'AstraZeneca', 'value': 'AstraZeneca'}, {'label': 'Moderna','value': 'Moderna'},
  ];

  final List<Map<String, dynamic>> pilihanKota = [
    {'label': "Jakarta", 'value': "Jakarta"}, {'label': "Bogor", 'value': "Bogor"},
    {'label': "Depok", 'value': "Depok"}, {'label': "Tangerang",'value': "Tangerang"},
    {'label': "Bekasi", 'value': "Bekasi"},
  ];

  Future<LokasiV> createLokasi(String Nama, String Lokasi, String Kota, String JenisVaksin, String Jadwal, String LinkDaftar, String Foto)
  async {
    List<LokasiVaksin> lokasiVaksinn = [];
    LokasiVaksin lokasiBaru = LokasiVaksin(Nama: Nama, Lokasi: Lokasi, Kota: Kota, JenisVaksin: JenisVaksin, Jadwal: Jadwal, LinkDaftar: LinkDaftar, Foto: Foto);
    lokasiVaksinn.add(lokasiBaru);
    LokasiV data = LokasiV(lokasiVaksin: lokasiVaksinn);

    final response = await http.post(
      Uri.parse('https://lindungipeduli.herokuapp.com/daftar-vaksin/data'),
      headers: <String, String>{ 'Content-Type': 'application/json; charset=UTF-8' },
      body: LokasiVaksinToJson(data),
    );

    if (response.statusCode == 200) {
      Navigator.of(context).pop();
      return LokasiV.fromJson(jsonDecode(response.body));
    }
    else {
      throw Exception('Failed');
    }
  }

  String nama = "";
  String lokasi = "";
  String kota = "";
  String jenis = "";
  String jadwal = "";
  String linkdaftar = "";
  String foto = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Tambah Lokasi Vaksin'),
        backgroundColor: Colors.blueGrey,
      ),
      body: Container(
        padding: const EdgeInsets.all(20.0),
        child: Form(
          key: _formKey,
          child: Scrollbar(
            isAlwaysShown: true,
            child: ListView(
              padding: const EdgeInsets.only(right: kFloatingActionButtonMargin),
              children: [
                SizedBox(
                  height: 50,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const <Widget>[
                      Text("Tambah", style: TextStyle(
                          fontSize: 30,
                          color: Colors.amber,
                          fontWeight: FontWeight.w700),
                      ),
                      Text("Lokasi", style: TextStyle(
                          fontSize: 30,
                          color: Colors.blue,
                          fontWeight: FontWeight.w700),
                      ),
                    ],
                  ),
                ),
                TextFormField(
                  decoration: const InputDecoration(
                      labelText: "Nama Lokasi",
                      icon: Icon(Icons.local_hospital),
                      border:  OutlineInputBorder(),
                      errorBorder: OutlineInputBorder( borderSide: BorderSide(color: Colors.red, width: 2))
                  ),
                  onSaved: (value) {
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Masukkan Nama Lokasi';
                    }
                    nama = value;
                  },
                ),
                const SizedBox(height: 15),
                SelectFormField(
                    decoration: const InputDecoration(
                      icon: Icon(Icons.medication_rounded ),
                      labelText: "Jenis Vaksin",
                      border: OutlineInputBorder(),
                    ),
                    items: pilihanJenis,
                    onChanged: (val) => setState(() {
                      jenis = val;
                    })
                ),
                const SizedBox(height: 15),
                SelectFormField(
                    decoration: const InputDecoration(
                      icon: Icon(Icons.location_city_rounded ),
                      labelText: "Kota",
                      border: OutlineInputBorder(),
                    ),
                    items: pilihanKota,
                    onChanged: (val) => setState(() {
                      kota = val;
                    })
                ),
                const SizedBox(height: 15),
                TextFormField(
                  decoration: const InputDecoration(
                      icon: Icon(Icons.add_location_alt_rounded),
                      labelText: "Lokasi",
                      border:  OutlineInputBorder(),
                      errorBorder: OutlineInputBorder( borderSide: BorderSide(color: Colors.red, width: 2))
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Masukkan Daerah Lokasi';
                    }
                    lokasi = value;
                  },
                ),
                const SizedBox(height: 15),
                TextFormField(
                  decoration: const InputDecoration(
                      icon: Icon(Icons.calendar_today_rounded),
                      labelText: "Jadwal",
                      border:  OutlineInputBorder(),
                      errorBorder: OutlineInputBorder( borderSide: BorderSide(color: Colors.red, width: 2))
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Masukkan Jadwal Vaksin';
                    }
                    jadwal = value;
                  },
                  onSaved: (value) {
                  },
                ),
                const SizedBox(height: 15),
                TextFormField(
                  decoration: const InputDecoration(
                      icon: Icon(Icons.insert_link_rounded),
                      labelText: "Link Pendaftaran",
                      border:  OutlineInputBorder(),
                      errorBorder: OutlineInputBorder( borderSide: BorderSide(color: Colors.red, width: 2))
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Masukkan Link Pendaftaran';
                    }
                    linkdaftar = value;
                  },
                  onSaved: (value) {
                  },
                ),
                const SizedBox(height: 15),
                TextFormField(
                  decoration: const InputDecoration(
                      icon: Icon(Icons.insert_photo_rounded),
                      labelText: "Link Foto",
                      border:  OutlineInputBorder(),
                      errorBorder: OutlineInputBorder( borderSide: BorderSide(color: Colors.red, width: 2))
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Masukkan Link Foto';
                    }
                    foto = value;
                  },
                  onSaved: (value) {
                  },
                ),
                const SizedBox(height: 20),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.amber,
                  ),
                  onPressed: () async {
                    final validated = _formKey.currentState!.validate();
                    if (validated){
                      _formKey.currentState!.save();
                      createLokasi(nama, lokasi, kota, jenis, jadwal, linkdaftar, foto);
                    }
                    else {
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(content: Text('Penambahan Lokasi Gagal')),
                      );
                    }
                  },
                  child: const Text('Submit'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}