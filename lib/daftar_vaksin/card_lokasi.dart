import 'package:flutter/material.dart';
import 'package:uas_project/daftar_vaksin/lokasi_vaksin.dart';
import 'package:uas_project/daftar_vaksin/detail_lokasi.dart';

Widget cardLokasi(lokasi, context, onGoBack) {
  return Card(
    shadowColor: Colors.black38,
    margin: const EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 8.0),
    child: Padding(
      padding: const EdgeInsets.all(15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30.0),
                image: DecorationImage(
                    image: NetworkImage(lokasi.Foto,), fit: BoxFit.cover),
                ),
                height: 200,
                width: 300,
            ),
          const SizedBox(height: 10),
          Text(lokasi.Nama, style: const TextStyle(
              fontSize: 20.0,
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 5.0),
          Text(lokasi.Lokasi + ", " + lokasi.Kota, style: const TextStyle(
              fontSize: 14.0,
              color: Colors.grey,
              fontStyle: FontStyle.italic,
            ),
          ),
          ElevatedButton(

            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => DetailPage(lokasiVaksin: lokasi)),
              ).then(onGoBack);
            },
            child: const Text('Detail', style: TextStyle(
              fontSize: 16.0,
              color: Colors.white,
              ),
            ),
            style: ElevatedButton.styleFrom(
              // fixedSize: const Size(5, 15),
              primary: Colors.blue,
              onSurface: Colors.amber,
            ),
          ),
        ],
      ),
    ),
  );
}