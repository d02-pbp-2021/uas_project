import 'dart:convert';
import 'dart:async';
import 'dart:io';
import 'dart:ui';
import 'package:flutter/material.dart';
import '../widgets/drawer.dart';
import 'package:http/http.dart' as http;
import 'package:uas_project/daftar_vaksin/lokasi_vaksin.dart';
import 'package:url_launcher/url_launcher.dart';

class DetailPage extends StatelessWidget {
  final LokasiVaksin lokasiVaksin;
  const DetailPage({Key? key, required this.lokasiVaksin}) : super(key: key);

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    }
    else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Lokasi Vaksin"),
        backgroundColor: Colors.blueGrey,
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30.0),
                  image: DecorationImage(
                      image: NetworkImage(lokasiVaksin.Foto,), fit: BoxFit.cover),
                ),
                height: 300,
                width: 500,
              ),
              const SizedBox(height: 10),
              Center (
                child: Column (
                  children: <Widget>[
                    Text(lokasiVaksin.Nama, style: const TextStyle(
                      fontSize: 22.0,
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      ),
                      ),
                    const SizedBox(height: 5.0),
                    Text(lokasiVaksin.Lokasi +", " + lokasiVaksin.Kota, style: const TextStyle(
                      fontSize: 18.0,
                      color: Colors.grey,
                      fontStyle: FontStyle.italic,
                    ),
                    ),
                    const SizedBox(height: 15),
                    Wrap(
                      crossAxisAlignment: WrapCrossAlignment.center,
                      children: [
                        const Icon(Icons.medication_rounded ),
                        Text(" " + lokasiVaksin.JenisVaksin, style: const TextStyle(
                          fontSize: 20,
                          color: Colors.black,
                        ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 15),
                    Wrap(
                      crossAxisAlignment: WrapCrossAlignment.center,
                      children: const [
                        Icon(Icons.calendar_today_rounded),
                        Text(" Jadwal Vaksin", style: TextStyle(
                          fontSize: 20,
                          color: Colors.black,
                        ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),
                    Text(lokasiVaksin.Jadwal, style: const TextStyle(
                      fontSize: 16.0,
                      color: Colors.black,
                      ),
                    ),
                ],
              ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          _launchURL(lokasiVaksin.LinkDaftar);
        },
        icon: const Icon(Icons.local_hospital_rounded ),
        label: const Text("Daftar Vaksin"),
        shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16.0))),
        backgroundColor: Colors.amber,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}