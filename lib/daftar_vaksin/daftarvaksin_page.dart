import 'dart:convert';
import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import '../widgets/drawer.dart';
import 'package:http/http.dart' as http;
import 'package:uas_project/daftar_vaksin/lokasi_vaksin.dart';
import 'package:uas_project/daftar_vaksin/card_lokasi.dart';
import 'package:uas_project/daftar_vaksin/lokasi_vaksin_form.dart';
import 'package:uas_project/pengaturan_pengguna/login_page.dart';

class DaftarVaksin extends StatelessWidget {
  const DaftarVaksin({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
        home: Scaffold(
          body: CardLokasi(),
        )
    );
  }
}

class CardLokasi extends StatefulWidget {
  const CardLokasi({Key? key}) : super(key: key);

  @override
  CardLokasiState createState() {
    return CardLokasiState();
  }
}

class CardLokasiState extends State<CardLokasi> {

  Future<List<LokasiVaksin>> _fetchData(String query) async {
    var url = Uri.parse(
        'https://lindungipeduli.herokuapp.com/daftar-vaksin/data');
    var response =
    await http.get(url);

    List<LokasiVaksin> lokasi = LokasiVaksinFromJson(response.body).lokasiVaksin;
    lokasi = lokasi.where((tes) {
    final namaLower = tes.Nama.toLowerCase();
    final lokasiLower = tes.Lokasi.toLowerCase();
    final kotaLower = tes.Kota.toLowerCase();
    final queryLower = query.toLowerCase();

    return namaLower.contains(queryLower) || lokasiLower.contains(queryLower) || kotaLower.contains(queryLower);
    }).toList();

    return lokasi;
  }

  final _controller = TextEditingController();
  String query = "";
  List<LokasiVaksin> _lokasiList = [];
  Timer? debouncer;

  // Source: https://github.com/JohannesMilke/filter_listview_example
  // https://www.youtube.com/watch?v=oFZIwBudIj0
  @override
  void initState() {
    stateAwal();
  }

  void debounce(
      VoidCallback callback, {
        Duration duration = const Duration(milliseconds: 1000),
      }) {
    if (debouncer != null) {
      debouncer!.cancel();
    }
    debouncer = Timer(duration, callback);
  }

  Future searchLokasi(String search) async => debounce(() async {
    final lokasiVaksin = await _fetchData(search);

    setState(() {
      this.query = query;
      _lokasiList = lokasiVaksin;
    });
  });

  FutureOr onGoBack(dynamic value) {
    stateAwal();
  }

  Future stateAwal() async => debounce(() async {
    if (!mounted) return;
    final lokasiVac = await _fetchData(query);

    setState(() {
      _lokasiList = lokasiVac;
    });
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Lokasi Vaksin"),
          backgroundColor: Colors.blueGrey,
        ),
        backgroundColor: Colors.white,
        drawer: const MyDrawer(),
        body: Column(
            children: <Widget>[
              SizedBox(
                height: 65,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    Text( "Lokasi", style: TextStyle(
                          fontSize: 30,
                          color: Colors.amber,
                          fontWeight: FontWeight.w700),
                    ),
                    Text( "Vaksin", style: TextStyle(
                          fontSize: 30,
                          color: Colors.blue,
                          fontWeight: FontWeight.w700),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.all(5),
                child: TextField(
                  controller: _controller,
                  onChanged: (text) {
                    searchLokasi(text);
                  },
                  decoration: InputDecoration(
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
                      prefixIcon: const Icon(Icons.search, color: Colors.blueGrey),
                      suffixIcon: IconButton(
                          icon: const Icon(Icons.clear, color: Colors.blueGrey),
                          onPressed: () {
                            _controller.clear();
                            searchLokasi("");
                          }
                      ),
                      hintText: 'Cari Lokasi Vaksin',)
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: FutureBuilder<List<LokasiVaksin>>(
                    future: _fetchData(query),
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return Scrollbar(
                            isAlwaysShown: true,
                            child: ListView.builder(
                                itemCount: _lokasiList.length,
                                itemBuilder: (context, index) {
                                  return cardLokasi(_lokasiList[index], context, onGoBack);
                                })
                        );
                      } else if (snapshot.hasError) {
                        return Text('${snapshot.error}');
                      }

                      return Center(child: Column(
                        children: const [
                          SizedBox(height: 100),
                          Text("Loading Lokasi Vaksin"),
                          SizedBox (height: 20),
                          SizedBox( width: 30, height: 30, child: CircularProgressIndicator()),
                        ],
                      ),
                      );
                    },
                  ),
                ),
              )
            ],
        ),
        floatingActionButton: LoginPage.roles == "Admin" ? FloatingActionButton(
          onPressed: () =>
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const FormLokasiVaksin()),
              ).then(onGoBack),
          child: const Icon(Icons.add),
          backgroundColor: Colors.amber,
        ) : null,
    );
  }
}