import 'dart:convert';

// Source: https://docs.flutter.dev/cookbook/networking/send-data

LokasiV LokasiVaksinFromJson(String str) => LokasiV.fromJson(json.decode(str));
String LokasiVaksinToJson(LokasiV data) => json.encode(data.toJson());

class LokasiV {
  LokasiV({
    required this.lokasiVaksin,
  });

  List<LokasiVaksin> lokasiVaksin;

  factory LokasiV.fromJson(Map<String, dynamic> json) => LokasiV(
    lokasiVaksin: List<LokasiVaksin>.from(json["lokasiVaksin"].map((x) => LokasiVaksin.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "lokasiVaksin": lokasiVaksin[0].toJson(),
  };
}

class LokasiVaksin {
  final String Nama;
  final String Lokasi;
  final String Kota;
  final String JenisVaksin;
  final String Jadwal;
  final String LinkDaftar;
  final String Foto;

  LokasiVaksin({
    required this.Nama,
    required this.Lokasi,
    required this.Kota,
    required this.JenisVaksin,
    required this.Jadwal,
    required this.LinkDaftar,
    required this.Foto,
  });

  factory LokasiVaksin.fromJson(Map<String, dynamic> json) => LokasiVaksin(
      Nama: json['Nama'],
      Lokasi: json['Lokasi'],
      Kota: json['Kota'],
      JenisVaksin: json['JenisVaksin'],
      Jadwal: json['Jadwal'],
      LinkDaftar: json['LinkDaftar'],
      Foto: json['Foto'],
    );

  Map<String, dynamic> toJson() => {
    "Nama": Nama,
    "Lokasi": Lokasi,
    "Kota": Kota,
    "JenisVaksin": JenisVaksin,
    "Jadwal": Jadwal,
    "LinkDaftar": LinkDaftar,
    "Foto": Foto,
  };
}