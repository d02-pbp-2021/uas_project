# PROJECT UAS D02 PBP 2021

## Daftar isi

- [Daftar Isi](#daftar-isi)
- [Anggota Kelompok](#anggota-kelompok)
- [Link Download Aplikasi Mobile](#link-download-aplikasi)
- [Aplikasi yang Diajukan](#aplikasi-yang-diajukan)
- [Persona Pengguna](#persona-pengguna)
- [Integrasi dengan Web Service](#integrasi-dengan-web-service)

## Anggota Kelompok
| NAMA                          |     NPM    |
| ----------------------------- | ---------- |
| Arya Bintang Pratama K.       | 2006462960 |
| Judah Ariesaka Magaini        | 2006463042 |
| Kenneth Jonathan              | 2006463364 |
| Farah Dita Ashilah            | 2006463723 |
| David Johan Hasiholan P.      | 2006482211 |
| Adristi Marsalma Nectarine A. | 2006483113 |

## Link Download Aplikasi
- **Link download Lindungi Peduli APK**: https://ristek.link/LindungiPeduliMobileAPK
- **Link website Lindungi Peduli**    : https://lindungipeduli.herokuapp.com/

## Aplikasi yang Diajukan
1. **Tema**		: Aplikasi E-Health
2. **Judul**	: Lindungi Peduli
3. **Manfaat**	:
   Sebagai wadah yang menyediakan informasi seputar Covid-19, termasuk informasi terkini, pendaftaran vaksin, apotek daring sebagai penyedia obat-obatan, hingga informasi kesehatan lain yang dikemas dalam bentuk artikel. 
4. **Daftar Modul** :
   - Pengaturan Pengguna
      - **Developer** : Arya Bintang Pratama K. 
      - **Deskripsi** : Fitur Login dan Register akun, dengan 3 role yaitu Author, Customer, Seller. terdapat authentication untuk membedakan role apa saja yang bisa masuk ke page tertentu.
      - **Web Service** : Modul akan mengakses web service Django untuk proses authentication dengan menggunakan link API.
      - **Detail Fitur** : Arya Bintang Pratama K. 
        - Login Page
        - Register Page
        - Authentication User
   - Home Page
      - **Developer** : Kenneth Jonathan
      - **Deskripsi** : Home Page menampilkan informasi seputar COVID-19, serta topik-topik diskusi yang dihubungkan dengan forum untuk berdiskusi terkait topik tersebut.
      - **Web Service** : Modul akan menggunakan JsonResponse untuk mengambil data Models dari database yang akan ditampilkan (Thread dan Replies). Pengambilan data tersebut akan diimplementasikan menggunakan fungsi Views pada Django. Kemudian pengiriman data dari aplikasi akan menggunakan REST API, dengan method-method seperti POST dan GET.
      - **Detail Fitur** :
         - Landing Page (Kondisi COVID-19 dan Topik-topik diskusi)
         - Halaman forum diskusi
         - Halaman untuk menambah topik forum
   3. Fitur Artikel
      - **Developer**: Judah Ariesaka Magaini
      - **Deskripsi**: Fitur ini menyediakan artikel-artikel tentang kesehatan. Setiap pengunjung dapat membaca dan mencari artikel, namun hanya pengguna yang telah login yang dapat menyukai artikel dan memberi komentar. Untuk menulis dan menghapus artikel, pengguna harus login sebagai 'Author' terlebih dahulu. Hanya admin yang dapat menambahkan kategori artikel.
      - **Web Service** : Modul akan mengakses username dari author melalui web service di Django untuk authetication serta mengecek artikel mana yang ditulis oleh author tersebut. Halaman artikel serta halaman detail setiap artikel akan diakses melalui link API yang diintegrasikan dengan Django dalam bentuk JSON atau XML.
      - **Detail fitur**:
         - Tampilan utama artikel
         - Halaman detail artikel
         - Fitur comment dan like pada setiap artikel
         - Pencarian artikel berdasarkan kata kunci dan kategori
   4. Kamus Obat
      - **Developer**: Farah Dita Ashilah
      - **Deskripsi**: Fitur ini menyediakan tampilan mengenai obat-obatan yang disertai dengan deskripsi, efek samping, dan komposisi obat. Fitur ini dilengkapi dengan search bar dan load more pagination. Untuk menambahkan deskripsi obat 
      atau menghapus kamus obat, pengguna perlu log in terlebih dahulu sebagai admin.
      - **Web Service** : Modul akan melakukan authetication untuk mengisi form dengan mengakses web service pada Django. Setiap input dari form akan dikirim kepada database Django dan ditampilkan dengan mengakses API yang telah diintegrasikan kepada web service Django.
      - **Detail fitur**:
            - Tampilan utama daftar kamus obat
            - Halaman detail kamus obat
            - Form untuk menambahkan kamus obat dan menghapus kamus obat
   5. Apotek Online
      - **Developer**: Adristi Marsalma Nectarine Amaranthi
      - **Deskripsi**: Fitur ini menyediakan tampilan produk apotek dengan deskirpsi singkat dan link pembelian obat. Fitur ini dilengkapi dengan search bar. Hanya pengguna dengan role Seller dan Admin saja yang dapat menambahkan obat.
      - **Web Service** : Modul akan mengakses web service untuk menampilkan setiap card produk dan detail setiap produk di flutter melalui REST API yang telah dibuat pada Django.
      - **Detail fitur**:
         - Tampilan halaman katalog apotek
         - Form untuk menambahkan produk
   6. Daftar Tes Covid : 
      - **Developer**: Melati Eka Putri
      - **Deskripsi**: Fitur ini menyedikan pilihan tes covid yang dapat difilter sesuai tipe dan lokasi tes covid yang dipilih. Pengguna juga bisa menggunakan fitur pencarian untuk mencari tes covid berdasarkan judul atau lokasi. Untuk melihat detail tes covid, pengguna harus login sebagai 'Customer' terlebih dahulu. Hanya admin yang dapat menambahkan dan menghapus tes covid.
      - **Web Service** : Modul flutter akan mengakses API dari Django yang dibuat menggunakan DJANGO REST FRAMEWORK. Fungsi yang diimplementasikan adalah get, post, dan delete. 
      - **Detail fitur**:
         - Tampilan utama daftar tes covid
         - Filter tes covid berdasarkan tipe dan lokasi
         - Pencarian tes covid berdasarkan judul atau lokasi
         - Halaman detail tes covid
         - Form untuk menambahkan daftar tes covid
   7. Pendaftaran Vaksin 
      - **Developer**: David Johan Hasiholan Parhusip
      - **Deskripsi**: Fitur ini menyedikan pilihan lokasi vaksin yang dapat difilter sesuai tipe vaksin dan kota yang diinginkan. Lokasi pendaftaran vaksin juga dapat dicari berdasarkan nama dari lokasi yang diinginkan. Hanya admin yang dapat menambahkan dan menghapus lokasi vaksin.
      - **Web Service** : Modul akan mengakses web service Django melalui url yang telah dibuat pada views.py dalam bentuk JSON, dan menampilkan setiap datanya pada flutter.
      - **Detail fitur**:
         - Tampilan utama daftar lokasi vaksin
         - Form untuk menambahkan lokasi vaksin
         - Filter lokasi berdasarkan tipe vaksin dan kota
         - Kotak pencarian lokasi berdasarkan nama

## Persona Pengguna
Berikut merupakan persona dari pengguna situs web Lindungi Peduli:
   - Visitor: Pengguna yang belum login, memiliki akses yang paling dibatasi
   - Login User: Pengguna yang sudah login dan bisa mengakses semua fitur. Namun tidak punya otoritas khusus
   - Article Author: Pengguna khusus yang berperan sebagai penulis artikel pada situs web
   - Seller: Pengguna khusus yang berperan sebagai penyedia obat-obatan yang dijual pada situs web
   - Admin: Pengguna yang memiliki akses ke semua fitur (baik otoritas khusus atau bukan) dan memiliki wewenang untuk menghapus suatu konten/produk

**Akses setiap persona**:
| FITUR                           |    AKSES                                            |
| -----------------------------   | --------------------------------------------------  |
| Halaman Login                   | Visitor, Login User, Article Author, Seller, Admin  |
| Halaman Register                | Visitor, Admin                                      |
| Fitur penambahan obat           | Seller, Admin                                       |
| Mengontrol obat yang dijual     | Admin                                               |
| Melihat kamus dan katalog obat  | Visitor, Login User, Article Author, Seller, Admin  |
| Melihat halaman utama tes covid | Visitor, Login User, Article Author, Seller, Admin  |
| Melihat halaman detail tes covid| Login User, Article Author, Seller, Admin           |
| Menulis artikel                 | Article Author, Admin                               |
| Mengontrol artikel terpublikasi | Admin                                               |
| Melihat artikel                 | Visitor, Login User, Article Author, Seller, Admin  |
| Comment dan Likes pada artikel  | Login User, Article Author, Seller, Admin           |
| Melihat halaman utama vaksin    | Visitor, Login User, Article Author, Seller, Admin  |
| Melihat detail vaksin           | Login User, Article Author, Seller, Admin           |

## Integrasi dengan Web Service
Secara umum, langkah-langkah yang dilakukan untuk setiap modul adalah:
1. Mengintegrasikan backend web service Django dengan flutter menggunakan REST API
2. Setiap modul akan mengimplementasikan pengiriman dan pengambilan data JSON dengan menggunakan JsonResponse
3. Data akan diterima oleh flutter dan dimasukkan ke dalam representasi tipe data Future karena menggunakan http asynchronous
4. Untuk setiap data yang terintegrasi dengan model tertentu di flutter, perlu ditambahkan method Models.toJson untuk mengirim data ke Django dan Models.fromJson untuk mengambil data dari Django
5. Desain interface aplikasi disesuaikan dengan implementasi desain yang ada pada website, yaitu menggunakan widget-widget milik flutter.

